REM ---- android packager will output
REM ---- prepackaged files to build into
REM ---- build/android.
REM ---- Import this structure into eclipse.


REM -- Clear expect files from folders first.

call androidclean.bat

REM -- List and copy data to the assets folder
cd android\pinball\src\main
IF NOT EXIST assets mkdir assets

cd ..
cd ..
cd ..
cd ..
cd ..
cd data
cd

copy anim\* ..\build\android\pinball\src\main\assets\
copy object\* ..\build\android\pinball\src\main\assets\
copy scenery\* ..\build\android\pinball\src\main\assets\
copy sound\* ..\build\android\pinball\src\main\assets\
IF NOT EXIST ..\build\android\pinball\src\main\assets\config mkdir ..\build\android\pinball\src\main\assets\config
IF NOT EXIST ..\build\android\pinball\src\main\assets\config\base mkdir ..\build\android\pinball\src\main\assets\config\base
IF NOT EXIST ..\build\android\pinball\src\main\assets\config\rasterblaster mkdir ..\build\android\pinball\src\main\assets\config\rasterblaster
copy config\base\* ..\build\android\pinball\src\main\assets\config\base\
copy config\rasterblaster\* ..\build\android\pinball\src\main\assets\config\rasterblaster\
REM pause

REM -- Concat all located ini files into the assets folder
cd ..
cd bin
cd windows
copy pinball.ini ..\..\build\android\pinball\src\main\assets\orxd.ini
copy pinball.ini ..\..\build\android\pinball\src\main\assets\orx.ini 
REM copy ..\build\android\app\src\main\assets\orxd.ini ..\build\android\app\src\main\assets\orx.ini <!--- up to here!

REM -- Copy source files to the jni folder
cd ..
cd ..
cd src
cd pinball

copy *.cpp ..\..\build\android\pinball\src\jni\
copy *.h ..\..\build\android\pinball\src\jni\

cd ..
cd ..
cd build
