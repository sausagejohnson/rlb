LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

LOCAL_MODULE    := rasterBlasterReloadedModule

LOCAL_SRC_FILES := bumperelement.cpp clawelement.cpp directionrolloverelement.cpp element.cpp elementgroup.cpp indicators.cpp laneelement.cpp pinballbase.cpp plunger.cpp rasterblaster.cpp rolloverlightelement.cpp shieldelement.cpp slingshotelement.cpp spinnerelement.cpp targetrightelement.cpp targettopelement.cpp _pinball.cpp
LOCAL_STATIC_LIBRARIES := orx

LOCAL_ARM_MODE := arm

include $(BUILD_SHARED_LIBRARY)

$(call import-module,lib/static/android)

