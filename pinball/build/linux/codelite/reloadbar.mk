##
## Auto Generated makefile by CodeLite IDE
## any manual changes will be erased      
##
## Release_x64
ProjectName            :=reloadbar
ConfigurationName      :=Release_x64
WorkspacePath          :=/home/sausage/Documents/rlb/pinball/build/linux/codelite
ProjectPath            :=/home/sausage/Documents/rlb/pinball/build/linux/codelite
IntermediateDirectory  :=$(ConfigurationName)
OutDir                 := $(IntermediateDirectory)
CurrentFileName        :=
CurrentFilePath        :=
CurrentFileFullPath    :=
User                   :=sausage
Date                   :=09/06/20
CodeLitePath           :=/home/sausage/.codelite
LinkerName             :=/usr/bin/g++
SharedObjectLinkerName :=/usr/bin/g++ -shared -fPIC
ObjectSuffix           :=.o
DependSuffix           :=.o.d
PreprocessSuffix       :=.i
DebugSwitch            :=-g 
IncludeSwitch          :=-I
LibrarySwitch          :=-l
OutputSwitch           :=-o 
LibraryPathSwitch      :=-L
PreprocessorSwitch     :=-D
SourceSwitch           :=-c 
OutputFile             :=../../../bin/linux64/reloadbar
Preprocessors          :=
ObjectSwitch           :=-o 
ArchiveOutputSwitch    := 
PreprocessOnlySwitch   :=-E
ObjectsFileList        :="reloadbar.txt"
PCHCompileFlags        :=
MakeDirCommand         :=mkdir -p
LinkOptions            :=  -static-libgcc -static-libstdc++ -m64 -L/usr/lib64 -Wl,-rpath ./ -Wl,--export-dynamic
IncludePath            :=  $(IncludeSwitch). $(IncludeSwitch)../../../include $(IncludeSwitch)$(ORX)/include 
IncludePCH             := 
RcIncludePath          := 
Libs                   := $(LibrarySwitch)orx $(LibrarySwitch)dl $(LibrarySwitch)m $(LibrarySwitch)rt $(LibrarySwitch)hid $(LibrarySwitch)udev 
ArLibs                 :=  "orx" "dl" "m" "rt" "hid" "udev" 
LibPath                := $(LibraryPathSwitch). $(LibraryPathSwitch)../../../lib $(LibraryPathSwitch)$(ORX)/lib/dynamic $(LibraryPathSwitch). 

##
## Common variables
## AR, CXX, CC, AS, CXXFLAGS and CFLAGS can be overriden using an environment variables
##
AR       := /usr/bin/ar rcu
CXX      := /usr/bin/g++
CC       := /usr/bin/gcc
CXXFLAGS :=  -msse2 -ffast-math -g -O2 -m64 -fno-exceptions -fno-rtti -fschedule-insns $(Preprocessors)
CFLAGS   :=  -msse2 -ffast-math -g -O2 -m64 -fschedule-insns $(Preprocessors)
ASFLAGS  := 
AS       := /usr/bin/as


##
## User defined environment variables
##
CodeLiteDir:=/usr/share/codelite
Objects0=$(IntermediateDirectory)/up_up_up_src_pinball_achievements.cpp$(ObjectSuffix) $(IntermediateDirectory)/up_up_up_src_pinball_beers.cpp$(ObjectSuffix) $(IntermediateDirectory)/up_up_up_src_pinball_bootstrap.cpp$(ObjectSuffix) $(IntermediateDirectory)/up_up_up_src_pinball_bumperelement.cpp$(ObjectSuffix) $(IntermediateDirectory)/up_up_up_src_pinball_bumpertargetelement.cpp$(ObjectSuffix) $(IntermediateDirectory)/up_up_up_src_pinball_directionrolloverelement.cpp$(ObjectSuffix) $(IntermediateDirectory)/up_up_up_src_pinball_element.cpp$(ObjectSuffix) $(IntermediateDirectory)/up_up_up_src_pinball_elementgroup.cpp$(ObjectSuffix) $(IntermediateDirectory)/up_up_up_src_pinball_finaltarget.cpp$(ObjectSuffix) $(IntermediateDirectory)/up_up_up_src_pinball_highscoreentry.cpp$(ObjectSuffix) \
	$(IntermediateDirectory)/up_up_up_src_pinball_indicators.cpp$(ObjectSuffix) $(IntermediateDirectory)/up_up_up_src_pinball_music.cpp$(ObjectSuffix) $(IntermediateDirectory)/up_up_up_src_pinball_pinballbase.cpp$(ObjectSuffix) $(IntermediateDirectory)/up_up_up_src_pinball_plunger.cpp$(ObjectSuffix) $(IntermediateDirectory)/up_up_up_src_pinball_reloadbar.cpp$(ObjectSuffix) $(IntermediateDirectory)/up_up_up_src_pinball_rolloverlightelement.cpp$(ObjectSuffix) $(IntermediateDirectory)/up_up_up_src_pinball_saucerelement.cpp$(ObjectSuffix) $(IntermediateDirectory)/up_up_up_src_pinball_shieldelement.cpp$(ObjectSuffix) $(IntermediateDirectory)/up_up_up_src_pinball_slingshotelement.cpp$(ObjectSuffix) $(IntermediateDirectory)/up_up_up_src_pinball_spinnerelement.cpp$(ObjectSuffix) \
	$(IntermediateDirectory)/up_up_up_src_pinball_targetelement.cpp$(ObjectSuffix) $(IntermediateDirectory)/up_up_up_src_pinball_targetrightelement.cpp$(ObjectSuffix) $(IntermediateDirectory)/up_up_up_src_pinball_targettopelement.cpp$(ObjectSuffix) $(IntermediateDirectory)/up_up_up_src_pinball_textdisplay.cpp$(ObjectSuffix) $(IntermediateDirectory)/up_up_up_src_pinball_usbreport.cpp$(ObjectSuffix) 



Objects=$(Objects0) 

##
## Main Build Targets 
##
.PHONY: all clean PreBuild PrePreBuild PostBuild MakeIntermediateDirs
all: $(OutputFile)

$(OutputFile): $(IntermediateDirectory)/.d $(Objects) 
	@$(MakeDirCommand) $(@D)
	@echo "" > $(IntermediateDirectory)/.d
	@echo $(Objects0)  > $(ObjectsFileList)
	$(LinkerName) $(OutputSwitch)$(OutputFile) @$(ObjectsFileList) $(LibPath) $(Libs) $(LinkOptions)

PostBuild:
	@echo Executing Post Build commands ...
	$(shell [ -f /home/sausage/Documents/orx/code/lib/dynamic/liborx.so ] && cp -f /home/sausage/Documents/orx/code/lib/dynamic/liborx*.so ../../../bin/linux64)
	@echo Done

MakeIntermediateDirs:
	@test -d $(ConfigurationName) || $(MakeDirCommand) $(ConfigurationName)


$(IntermediateDirectory)/.d:
	@test -d $(ConfigurationName) || $(MakeDirCommand) $(ConfigurationName)

PreBuild:


##
## Objects
##
$(IntermediateDirectory)/up_up_up_src_pinball_achievements.cpp$(ObjectSuffix): ../../../src/pinball/achievements.cpp $(IntermediateDirectory)/up_up_up_src_pinball_achievements.cpp$(DependSuffix)
	$(CXX) $(IncludePCH) $(SourceSwitch) "/home/sausage/Documents/rlb/pinball/src/pinball/achievements.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/up_up_up_src_pinball_achievements.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/up_up_up_src_pinball_achievements.cpp$(DependSuffix): ../../../src/pinball/achievements.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/up_up_up_src_pinball_achievements.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/up_up_up_src_pinball_achievements.cpp$(DependSuffix) -MM ../../../src/pinball/achievements.cpp

$(IntermediateDirectory)/up_up_up_src_pinball_achievements.cpp$(PreprocessSuffix): ../../../src/pinball/achievements.cpp
	$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/up_up_up_src_pinball_achievements.cpp$(PreprocessSuffix) ../../../src/pinball/achievements.cpp

$(IntermediateDirectory)/up_up_up_src_pinball_beers.cpp$(ObjectSuffix): ../../../src/pinball/beers.cpp $(IntermediateDirectory)/up_up_up_src_pinball_beers.cpp$(DependSuffix)
	$(CXX) $(IncludePCH) $(SourceSwitch) "/home/sausage/Documents/rlb/pinball/src/pinball/beers.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/up_up_up_src_pinball_beers.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/up_up_up_src_pinball_beers.cpp$(DependSuffix): ../../../src/pinball/beers.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/up_up_up_src_pinball_beers.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/up_up_up_src_pinball_beers.cpp$(DependSuffix) -MM ../../../src/pinball/beers.cpp

$(IntermediateDirectory)/up_up_up_src_pinball_beers.cpp$(PreprocessSuffix): ../../../src/pinball/beers.cpp
	$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/up_up_up_src_pinball_beers.cpp$(PreprocessSuffix) ../../../src/pinball/beers.cpp

$(IntermediateDirectory)/up_up_up_src_pinball_bootstrap.cpp$(ObjectSuffix): ../../../src/pinball/bootstrap.cpp $(IntermediateDirectory)/up_up_up_src_pinball_bootstrap.cpp$(DependSuffix)
	$(CXX) $(IncludePCH) $(SourceSwitch) "/home/sausage/Documents/rlb/pinball/src/pinball/bootstrap.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/up_up_up_src_pinball_bootstrap.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/up_up_up_src_pinball_bootstrap.cpp$(DependSuffix): ../../../src/pinball/bootstrap.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/up_up_up_src_pinball_bootstrap.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/up_up_up_src_pinball_bootstrap.cpp$(DependSuffix) -MM ../../../src/pinball/bootstrap.cpp

$(IntermediateDirectory)/up_up_up_src_pinball_bootstrap.cpp$(PreprocessSuffix): ../../../src/pinball/bootstrap.cpp
	$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/up_up_up_src_pinball_bootstrap.cpp$(PreprocessSuffix) ../../../src/pinball/bootstrap.cpp

$(IntermediateDirectory)/up_up_up_src_pinball_bumperelement.cpp$(ObjectSuffix): ../../../src/pinball/bumperelement.cpp $(IntermediateDirectory)/up_up_up_src_pinball_bumperelement.cpp$(DependSuffix)
	$(CXX) $(IncludePCH) $(SourceSwitch) "/home/sausage/Documents/rlb/pinball/src/pinball/bumperelement.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/up_up_up_src_pinball_bumperelement.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/up_up_up_src_pinball_bumperelement.cpp$(DependSuffix): ../../../src/pinball/bumperelement.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/up_up_up_src_pinball_bumperelement.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/up_up_up_src_pinball_bumperelement.cpp$(DependSuffix) -MM ../../../src/pinball/bumperelement.cpp

$(IntermediateDirectory)/up_up_up_src_pinball_bumperelement.cpp$(PreprocessSuffix): ../../../src/pinball/bumperelement.cpp
	$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/up_up_up_src_pinball_bumperelement.cpp$(PreprocessSuffix) ../../../src/pinball/bumperelement.cpp

$(IntermediateDirectory)/up_up_up_src_pinball_bumpertargetelement.cpp$(ObjectSuffix): ../../../src/pinball/bumpertargetelement.cpp $(IntermediateDirectory)/up_up_up_src_pinball_bumpertargetelement.cpp$(DependSuffix)
	$(CXX) $(IncludePCH) $(SourceSwitch) "/home/sausage/Documents/rlb/pinball/src/pinball/bumpertargetelement.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/up_up_up_src_pinball_bumpertargetelement.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/up_up_up_src_pinball_bumpertargetelement.cpp$(DependSuffix): ../../../src/pinball/bumpertargetelement.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/up_up_up_src_pinball_bumpertargetelement.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/up_up_up_src_pinball_bumpertargetelement.cpp$(DependSuffix) -MM ../../../src/pinball/bumpertargetelement.cpp

$(IntermediateDirectory)/up_up_up_src_pinball_bumpertargetelement.cpp$(PreprocessSuffix): ../../../src/pinball/bumpertargetelement.cpp
	$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/up_up_up_src_pinball_bumpertargetelement.cpp$(PreprocessSuffix) ../../../src/pinball/bumpertargetelement.cpp

$(IntermediateDirectory)/up_up_up_src_pinball_directionrolloverelement.cpp$(ObjectSuffix): ../../../src/pinball/directionrolloverelement.cpp $(IntermediateDirectory)/up_up_up_src_pinball_directionrolloverelement.cpp$(DependSuffix)
	$(CXX) $(IncludePCH) $(SourceSwitch) "/home/sausage/Documents/rlb/pinball/src/pinball/directionrolloverelement.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/up_up_up_src_pinball_directionrolloverelement.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/up_up_up_src_pinball_directionrolloverelement.cpp$(DependSuffix): ../../../src/pinball/directionrolloverelement.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/up_up_up_src_pinball_directionrolloverelement.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/up_up_up_src_pinball_directionrolloverelement.cpp$(DependSuffix) -MM ../../../src/pinball/directionrolloverelement.cpp

$(IntermediateDirectory)/up_up_up_src_pinball_directionrolloverelement.cpp$(PreprocessSuffix): ../../../src/pinball/directionrolloverelement.cpp
	$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/up_up_up_src_pinball_directionrolloverelement.cpp$(PreprocessSuffix) ../../../src/pinball/directionrolloverelement.cpp

$(IntermediateDirectory)/up_up_up_src_pinball_element.cpp$(ObjectSuffix): ../../../src/pinball/element.cpp $(IntermediateDirectory)/up_up_up_src_pinball_element.cpp$(DependSuffix)
	$(CXX) $(IncludePCH) $(SourceSwitch) "/home/sausage/Documents/rlb/pinball/src/pinball/element.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/up_up_up_src_pinball_element.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/up_up_up_src_pinball_element.cpp$(DependSuffix): ../../../src/pinball/element.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/up_up_up_src_pinball_element.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/up_up_up_src_pinball_element.cpp$(DependSuffix) -MM ../../../src/pinball/element.cpp

$(IntermediateDirectory)/up_up_up_src_pinball_element.cpp$(PreprocessSuffix): ../../../src/pinball/element.cpp
	$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/up_up_up_src_pinball_element.cpp$(PreprocessSuffix) ../../../src/pinball/element.cpp

$(IntermediateDirectory)/up_up_up_src_pinball_elementgroup.cpp$(ObjectSuffix): ../../../src/pinball/elementgroup.cpp $(IntermediateDirectory)/up_up_up_src_pinball_elementgroup.cpp$(DependSuffix)
	$(CXX) $(IncludePCH) $(SourceSwitch) "/home/sausage/Documents/rlb/pinball/src/pinball/elementgroup.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/up_up_up_src_pinball_elementgroup.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/up_up_up_src_pinball_elementgroup.cpp$(DependSuffix): ../../../src/pinball/elementgroup.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/up_up_up_src_pinball_elementgroup.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/up_up_up_src_pinball_elementgroup.cpp$(DependSuffix) -MM ../../../src/pinball/elementgroup.cpp

$(IntermediateDirectory)/up_up_up_src_pinball_elementgroup.cpp$(PreprocessSuffix): ../../../src/pinball/elementgroup.cpp
	$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/up_up_up_src_pinball_elementgroup.cpp$(PreprocessSuffix) ../../../src/pinball/elementgroup.cpp

$(IntermediateDirectory)/up_up_up_src_pinball_finaltarget.cpp$(ObjectSuffix): ../../../src/pinball/finaltarget.cpp $(IntermediateDirectory)/up_up_up_src_pinball_finaltarget.cpp$(DependSuffix)
	$(CXX) $(IncludePCH) $(SourceSwitch) "/home/sausage/Documents/rlb/pinball/src/pinball/finaltarget.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/up_up_up_src_pinball_finaltarget.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/up_up_up_src_pinball_finaltarget.cpp$(DependSuffix): ../../../src/pinball/finaltarget.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/up_up_up_src_pinball_finaltarget.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/up_up_up_src_pinball_finaltarget.cpp$(DependSuffix) -MM ../../../src/pinball/finaltarget.cpp

$(IntermediateDirectory)/up_up_up_src_pinball_finaltarget.cpp$(PreprocessSuffix): ../../../src/pinball/finaltarget.cpp
	$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/up_up_up_src_pinball_finaltarget.cpp$(PreprocessSuffix) ../../../src/pinball/finaltarget.cpp

$(IntermediateDirectory)/up_up_up_src_pinball_highscoreentry.cpp$(ObjectSuffix): ../../../src/pinball/highscoreentry.cpp $(IntermediateDirectory)/up_up_up_src_pinball_highscoreentry.cpp$(DependSuffix)
	$(CXX) $(IncludePCH) $(SourceSwitch) "/home/sausage/Documents/rlb/pinball/src/pinball/highscoreentry.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/up_up_up_src_pinball_highscoreentry.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/up_up_up_src_pinball_highscoreentry.cpp$(DependSuffix): ../../../src/pinball/highscoreentry.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/up_up_up_src_pinball_highscoreentry.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/up_up_up_src_pinball_highscoreentry.cpp$(DependSuffix) -MM ../../../src/pinball/highscoreentry.cpp

$(IntermediateDirectory)/up_up_up_src_pinball_highscoreentry.cpp$(PreprocessSuffix): ../../../src/pinball/highscoreentry.cpp
	$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/up_up_up_src_pinball_highscoreentry.cpp$(PreprocessSuffix) ../../../src/pinball/highscoreentry.cpp

$(IntermediateDirectory)/up_up_up_src_pinball_indicators.cpp$(ObjectSuffix): ../../../src/pinball/indicators.cpp $(IntermediateDirectory)/up_up_up_src_pinball_indicators.cpp$(DependSuffix)
	$(CXX) $(IncludePCH) $(SourceSwitch) "/home/sausage/Documents/rlb/pinball/src/pinball/indicators.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/up_up_up_src_pinball_indicators.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/up_up_up_src_pinball_indicators.cpp$(DependSuffix): ../../../src/pinball/indicators.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/up_up_up_src_pinball_indicators.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/up_up_up_src_pinball_indicators.cpp$(DependSuffix) -MM ../../../src/pinball/indicators.cpp

$(IntermediateDirectory)/up_up_up_src_pinball_indicators.cpp$(PreprocessSuffix): ../../../src/pinball/indicators.cpp
	$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/up_up_up_src_pinball_indicators.cpp$(PreprocessSuffix) ../../../src/pinball/indicators.cpp

$(IntermediateDirectory)/up_up_up_src_pinball_music.cpp$(ObjectSuffix): ../../../src/pinball/music.cpp $(IntermediateDirectory)/up_up_up_src_pinball_music.cpp$(DependSuffix)
	$(CXX) $(IncludePCH) $(SourceSwitch) "/home/sausage/Documents/rlb/pinball/src/pinball/music.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/up_up_up_src_pinball_music.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/up_up_up_src_pinball_music.cpp$(DependSuffix): ../../../src/pinball/music.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/up_up_up_src_pinball_music.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/up_up_up_src_pinball_music.cpp$(DependSuffix) -MM ../../../src/pinball/music.cpp

$(IntermediateDirectory)/up_up_up_src_pinball_music.cpp$(PreprocessSuffix): ../../../src/pinball/music.cpp
	$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/up_up_up_src_pinball_music.cpp$(PreprocessSuffix) ../../../src/pinball/music.cpp

$(IntermediateDirectory)/up_up_up_src_pinball_pinballbase.cpp$(ObjectSuffix): ../../../src/pinball/pinballbase.cpp $(IntermediateDirectory)/up_up_up_src_pinball_pinballbase.cpp$(DependSuffix)
	$(CXX) $(IncludePCH) $(SourceSwitch) "/home/sausage/Documents/rlb/pinball/src/pinball/pinballbase.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/up_up_up_src_pinball_pinballbase.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/up_up_up_src_pinball_pinballbase.cpp$(DependSuffix): ../../../src/pinball/pinballbase.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/up_up_up_src_pinball_pinballbase.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/up_up_up_src_pinball_pinballbase.cpp$(DependSuffix) -MM ../../../src/pinball/pinballbase.cpp

$(IntermediateDirectory)/up_up_up_src_pinball_pinballbase.cpp$(PreprocessSuffix): ../../../src/pinball/pinballbase.cpp
	$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/up_up_up_src_pinball_pinballbase.cpp$(PreprocessSuffix) ../../../src/pinball/pinballbase.cpp

$(IntermediateDirectory)/up_up_up_src_pinball_plunger.cpp$(ObjectSuffix): ../../../src/pinball/plunger.cpp $(IntermediateDirectory)/up_up_up_src_pinball_plunger.cpp$(DependSuffix)
	$(CXX) $(IncludePCH) $(SourceSwitch) "/home/sausage/Documents/rlb/pinball/src/pinball/plunger.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/up_up_up_src_pinball_plunger.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/up_up_up_src_pinball_plunger.cpp$(DependSuffix): ../../../src/pinball/plunger.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/up_up_up_src_pinball_plunger.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/up_up_up_src_pinball_plunger.cpp$(DependSuffix) -MM ../../../src/pinball/plunger.cpp

$(IntermediateDirectory)/up_up_up_src_pinball_plunger.cpp$(PreprocessSuffix): ../../../src/pinball/plunger.cpp
	$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/up_up_up_src_pinball_plunger.cpp$(PreprocessSuffix) ../../../src/pinball/plunger.cpp

$(IntermediateDirectory)/up_up_up_src_pinball_reloadbar.cpp$(ObjectSuffix): ../../../src/pinball/reloadbar.cpp $(IntermediateDirectory)/up_up_up_src_pinball_reloadbar.cpp$(DependSuffix)
	$(CXX) $(IncludePCH) $(SourceSwitch) "/home/sausage/Documents/rlb/pinball/src/pinball/reloadbar.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/up_up_up_src_pinball_reloadbar.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/up_up_up_src_pinball_reloadbar.cpp$(DependSuffix): ../../../src/pinball/reloadbar.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/up_up_up_src_pinball_reloadbar.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/up_up_up_src_pinball_reloadbar.cpp$(DependSuffix) -MM ../../../src/pinball/reloadbar.cpp

$(IntermediateDirectory)/up_up_up_src_pinball_reloadbar.cpp$(PreprocessSuffix): ../../../src/pinball/reloadbar.cpp
	$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/up_up_up_src_pinball_reloadbar.cpp$(PreprocessSuffix) ../../../src/pinball/reloadbar.cpp

$(IntermediateDirectory)/up_up_up_src_pinball_rolloverlightelement.cpp$(ObjectSuffix): ../../../src/pinball/rolloverlightelement.cpp $(IntermediateDirectory)/up_up_up_src_pinball_rolloverlightelement.cpp$(DependSuffix)
	$(CXX) $(IncludePCH) $(SourceSwitch) "/home/sausage/Documents/rlb/pinball/src/pinball/rolloverlightelement.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/up_up_up_src_pinball_rolloverlightelement.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/up_up_up_src_pinball_rolloverlightelement.cpp$(DependSuffix): ../../../src/pinball/rolloverlightelement.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/up_up_up_src_pinball_rolloverlightelement.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/up_up_up_src_pinball_rolloverlightelement.cpp$(DependSuffix) -MM ../../../src/pinball/rolloverlightelement.cpp

$(IntermediateDirectory)/up_up_up_src_pinball_rolloverlightelement.cpp$(PreprocessSuffix): ../../../src/pinball/rolloverlightelement.cpp
	$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/up_up_up_src_pinball_rolloverlightelement.cpp$(PreprocessSuffix) ../../../src/pinball/rolloverlightelement.cpp

$(IntermediateDirectory)/up_up_up_src_pinball_saucerelement.cpp$(ObjectSuffix): ../../../src/pinball/saucerelement.cpp $(IntermediateDirectory)/up_up_up_src_pinball_saucerelement.cpp$(DependSuffix)
	$(CXX) $(IncludePCH) $(SourceSwitch) "/home/sausage/Documents/rlb/pinball/src/pinball/saucerelement.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/up_up_up_src_pinball_saucerelement.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/up_up_up_src_pinball_saucerelement.cpp$(DependSuffix): ../../../src/pinball/saucerelement.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/up_up_up_src_pinball_saucerelement.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/up_up_up_src_pinball_saucerelement.cpp$(DependSuffix) -MM ../../../src/pinball/saucerelement.cpp

$(IntermediateDirectory)/up_up_up_src_pinball_saucerelement.cpp$(PreprocessSuffix): ../../../src/pinball/saucerelement.cpp
	$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/up_up_up_src_pinball_saucerelement.cpp$(PreprocessSuffix) ../../../src/pinball/saucerelement.cpp

$(IntermediateDirectory)/up_up_up_src_pinball_shieldelement.cpp$(ObjectSuffix): ../../../src/pinball/shieldelement.cpp $(IntermediateDirectory)/up_up_up_src_pinball_shieldelement.cpp$(DependSuffix)
	$(CXX) $(IncludePCH) $(SourceSwitch) "/home/sausage/Documents/rlb/pinball/src/pinball/shieldelement.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/up_up_up_src_pinball_shieldelement.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/up_up_up_src_pinball_shieldelement.cpp$(DependSuffix): ../../../src/pinball/shieldelement.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/up_up_up_src_pinball_shieldelement.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/up_up_up_src_pinball_shieldelement.cpp$(DependSuffix) -MM ../../../src/pinball/shieldelement.cpp

$(IntermediateDirectory)/up_up_up_src_pinball_shieldelement.cpp$(PreprocessSuffix): ../../../src/pinball/shieldelement.cpp
	$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/up_up_up_src_pinball_shieldelement.cpp$(PreprocessSuffix) ../../../src/pinball/shieldelement.cpp

$(IntermediateDirectory)/up_up_up_src_pinball_slingshotelement.cpp$(ObjectSuffix): ../../../src/pinball/slingshotelement.cpp $(IntermediateDirectory)/up_up_up_src_pinball_slingshotelement.cpp$(DependSuffix)
	$(CXX) $(IncludePCH) $(SourceSwitch) "/home/sausage/Documents/rlb/pinball/src/pinball/slingshotelement.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/up_up_up_src_pinball_slingshotelement.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/up_up_up_src_pinball_slingshotelement.cpp$(DependSuffix): ../../../src/pinball/slingshotelement.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/up_up_up_src_pinball_slingshotelement.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/up_up_up_src_pinball_slingshotelement.cpp$(DependSuffix) -MM ../../../src/pinball/slingshotelement.cpp

$(IntermediateDirectory)/up_up_up_src_pinball_slingshotelement.cpp$(PreprocessSuffix): ../../../src/pinball/slingshotelement.cpp
	$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/up_up_up_src_pinball_slingshotelement.cpp$(PreprocessSuffix) ../../../src/pinball/slingshotelement.cpp

$(IntermediateDirectory)/up_up_up_src_pinball_spinnerelement.cpp$(ObjectSuffix): ../../../src/pinball/spinnerelement.cpp $(IntermediateDirectory)/up_up_up_src_pinball_spinnerelement.cpp$(DependSuffix)
	$(CXX) $(IncludePCH) $(SourceSwitch) "/home/sausage/Documents/rlb/pinball/src/pinball/spinnerelement.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/up_up_up_src_pinball_spinnerelement.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/up_up_up_src_pinball_spinnerelement.cpp$(DependSuffix): ../../../src/pinball/spinnerelement.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/up_up_up_src_pinball_spinnerelement.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/up_up_up_src_pinball_spinnerelement.cpp$(DependSuffix) -MM ../../../src/pinball/spinnerelement.cpp

$(IntermediateDirectory)/up_up_up_src_pinball_spinnerelement.cpp$(PreprocessSuffix): ../../../src/pinball/spinnerelement.cpp
	$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/up_up_up_src_pinball_spinnerelement.cpp$(PreprocessSuffix) ../../../src/pinball/spinnerelement.cpp

$(IntermediateDirectory)/up_up_up_src_pinball_targetelement.cpp$(ObjectSuffix): ../../../src/pinball/targetelement.cpp $(IntermediateDirectory)/up_up_up_src_pinball_targetelement.cpp$(DependSuffix)
	$(CXX) $(IncludePCH) $(SourceSwitch) "/home/sausage/Documents/rlb/pinball/src/pinball/targetelement.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/up_up_up_src_pinball_targetelement.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/up_up_up_src_pinball_targetelement.cpp$(DependSuffix): ../../../src/pinball/targetelement.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/up_up_up_src_pinball_targetelement.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/up_up_up_src_pinball_targetelement.cpp$(DependSuffix) -MM ../../../src/pinball/targetelement.cpp

$(IntermediateDirectory)/up_up_up_src_pinball_targetelement.cpp$(PreprocessSuffix): ../../../src/pinball/targetelement.cpp
	$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/up_up_up_src_pinball_targetelement.cpp$(PreprocessSuffix) ../../../src/pinball/targetelement.cpp

$(IntermediateDirectory)/up_up_up_src_pinball_targetrightelement.cpp$(ObjectSuffix): ../../../src/pinball/targetrightelement.cpp $(IntermediateDirectory)/up_up_up_src_pinball_targetrightelement.cpp$(DependSuffix)
	$(CXX) $(IncludePCH) $(SourceSwitch) "/home/sausage/Documents/rlb/pinball/src/pinball/targetrightelement.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/up_up_up_src_pinball_targetrightelement.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/up_up_up_src_pinball_targetrightelement.cpp$(DependSuffix): ../../../src/pinball/targetrightelement.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/up_up_up_src_pinball_targetrightelement.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/up_up_up_src_pinball_targetrightelement.cpp$(DependSuffix) -MM ../../../src/pinball/targetrightelement.cpp

$(IntermediateDirectory)/up_up_up_src_pinball_targetrightelement.cpp$(PreprocessSuffix): ../../../src/pinball/targetrightelement.cpp
	$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/up_up_up_src_pinball_targetrightelement.cpp$(PreprocessSuffix) ../../../src/pinball/targetrightelement.cpp

$(IntermediateDirectory)/up_up_up_src_pinball_targettopelement.cpp$(ObjectSuffix): ../../../src/pinball/targettopelement.cpp $(IntermediateDirectory)/up_up_up_src_pinball_targettopelement.cpp$(DependSuffix)
	$(CXX) $(IncludePCH) $(SourceSwitch) "/home/sausage/Documents/rlb/pinball/src/pinball/targettopelement.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/up_up_up_src_pinball_targettopelement.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/up_up_up_src_pinball_targettopelement.cpp$(DependSuffix): ../../../src/pinball/targettopelement.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/up_up_up_src_pinball_targettopelement.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/up_up_up_src_pinball_targettopelement.cpp$(DependSuffix) -MM ../../../src/pinball/targettopelement.cpp

$(IntermediateDirectory)/up_up_up_src_pinball_targettopelement.cpp$(PreprocessSuffix): ../../../src/pinball/targettopelement.cpp
	$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/up_up_up_src_pinball_targettopelement.cpp$(PreprocessSuffix) ../../../src/pinball/targettopelement.cpp

$(IntermediateDirectory)/up_up_up_src_pinball_textdisplay.cpp$(ObjectSuffix): ../../../src/pinball/textdisplay.cpp $(IntermediateDirectory)/up_up_up_src_pinball_textdisplay.cpp$(DependSuffix)
	$(CXX) $(IncludePCH) $(SourceSwitch) "/home/sausage/Documents/rlb/pinball/src/pinball/textdisplay.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/up_up_up_src_pinball_textdisplay.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/up_up_up_src_pinball_textdisplay.cpp$(DependSuffix): ../../../src/pinball/textdisplay.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/up_up_up_src_pinball_textdisplay.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/up_up_up_src_pinball_textdisplay.cpp$(DependSuffix) -MM ../../../src/pinball/textdisplay.cpp

$(IntermediateDirectory)/up_up_up_src_pinball_textdisplay.cpp$(PreprocessSuffix): ../../../src/pinball/textdisplay.cpp
	$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/up_up_up_src_pinball_textdisplay.cpp$(PreprocessSuffix) ../../../src/pinball/textdisplay.cpp

$(IntermediateDirectory)/up_up_up_src_pinball_usbreport.cpp$(ObjectSuffix): ../../../src/pinball/usbreport.cpp $(IntermediateDirectory)/up_up_up_src_pinball_usbreport.cpp$(DependSuffix)
	$(CXX) $(IncludePCH) $(SourceSwitch) "/home/sausage/Documents/rlb/pinball/src/pinball/usbreport.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/up_up_up_src_pinball_usbreport.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/up_up_up_src_pinball_usbreport.cpp$(DependSuffix): ../../../src/pinball/usbreport.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/up_up_up_src_pinball_usbreport.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/up_up_up_src_pinball_usbreport.cpp$(DependSuffix) -MM ../../../src/pinball/usbreport.cpp

$(IntermediateDirectory)/up_up_up_src_pinball_usbreport.cpp$(PreprocessSuffix): ../../../src/pinball/usbreport.cpp
	$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/up_up_up_src_pinball_usbreport.cpp$(PreprocessSuffix) ../../../src/pinball/usbreport.cpp


-include $(IntermediateDirectory)/*$(DependSuffix)
##
## Clean
##
clean:
	$(RM) -r $(ConfigurationName)/


