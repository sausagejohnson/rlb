#ifndef SHIELDELEMENT_H
#define SHIELDELEMENT_H

class shieldelement : public element
{
private:
	orxBOOL shieldIsOn;
	
public:
	shieldelement(	orxSTRING activatorNameFromConfig, orxSTRING activatorAnimationName, 
					orxSTRING activatorHitAnimationName, orxSTRING lightNameFromConfig);
	~shieldelement();

	int RegisterHit();
	void TurnShieldOn();
	void TurnShieldOff();
	
	orxBOOL IsShieldOn();
};

#endif // SHIELDELEMENT_H
