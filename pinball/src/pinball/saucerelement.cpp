#include "orx.h"
#include "element.h"
#include "saucerelement.h"

saucerelement::saucerelement(int points) : element(points, (orxCHAR*)"SaucerObject", orxNULL) {
	activatorAnimationName = (orxCHAR*)"SaucerIdleOff";
	activatorHitAnimationName= orxNULL;
	
	ballIsCaptured = orxFALSE;
	capturedBall = orxNULL;
	ballCaptureTime = 0;
	
	kickoutMinimum = orxVECTOR_0;
	kickoutMaximum = orxVECTOR_0;
	
	ballLockClock = orxClock_Create(orx2F(2.0f), orxCLOCK_TYPE_USER);
	
	orxClock_Register(ballLockClock, BallCanBeReleasedDispatcher, this, orxMODULE_ID_MAIN, orxCLOCK_PRIORITY_NORMAL);
	orxClock_Pause(ballLockClock);
	
}

void orxFASTCALL saucerelement::BallCanBeReleasedDispatcher(const orxCLOCK_INFO *_pstClockInfo, void *_pstContext)
{
	saucerelement *thisSaucerElement = (saucerelement*)_pstContext;
	thisSaucerElement->BallCanBeReleased(_pstClockInfo, _pstContext);
}

saucerelement::~saucerelement()
{
	
	//do I need to clean up my clock?
	
}


void saucerelement::SetKickout(orxVECTOR minimum, orxVECTOR maximum){
	kickoutMinimum = minimum;
	kickoutMaximum = maximum;
}

orxBOOL saucerelement::IsObjectInitialised(orxOBJECT *object){
	if (object == orxNULL){
		return orxFALSE;
	}
	
	return orxTRUE;
}

void saucerelement::PlaySoundOn(orxOBJECT *object, orxSTRING soundFromConfig)
{
	if (!IsObjectInitialised(object)) {
		orxLOG("For some reason, this object is null.");
		return;
	}
	
	orxSOUND *currentSound = orxObject_GetLastAddedSound(object);
	if (currentSound != orxNULL){
		orxObject_RemoveSound(object, soundFromConfig);
	}
	orxObject_AddSound(object, soundFromConfig);
}

void saucerelement::SetCapturedBall(orxOBJECT *ball){
	orxLOG("BALL CAPTURED=========");
	
	capturedBall = ball;
	ballIsCaptured = orxTRUE;
	ballCaptureTime = (orxFLOAT)orxSystem_GetTime();
	
	orxClock_Restart(ballLockClock);
	orxClock_Unpause(ballLockClock);
	
	orxBODY *body = orxOBJECT_GET_STRUCTURE(capturedBall, BODY);
	orxBODY_PART *part = orxBody_GetNextPart(body, orxNULL);
	orxBody_SetPartSolid(part, orxFALSE);
	
	orxVECTOR zero = {0, 0, 0};
	
	orxObject_SetSpeed(capturedBall, &zero);
	orxObject_SetCustomGravity(capturedBall, &zero);
	
	orxVECTOR ballPosition = {0,0,0};
	orxObject_GetPosition(capturedBall, &ballPosition);
	
	orxObject_SetParent(capturedBall, activator); 		
	
	ballPosition.fX = 0;//104.887077
	ballPosition.fY = 0;//614.01947	
	orxObject_SetPosition(capturedBall, &ballPosition);
	
	PlaySoundOn(activator, "FlipperDownEffect");
	
}

void saucerelement::SetName(orxSTRING n){
	name = n;
}

orxSTRING saucerelement::GetName(){
	return name;
}

void saucerelement::BallCanBeReleased(const orxCLOCK_INFO *_pstClockInfo, void *_pstContext)
{
	orxLOG("DROP====");
	if (ballIsCaptured){
		orxClock_Pause(ballLockClock);
		ballIsCaptured = orxFALSE;
		DropCapturedBall();
	}
	
}


orxBOOL saucerelement::BallIsCaptured(){
	return ballIsCaptured;
}

void saucerelement::DropCapturedBall(){
	ballIsCaptured = orxFALSE;
	
	if (capturedBall != orxNULL){
		orxBODY *body = orxOBJECT_GET_STRUCTURE(capturedBall, BODY);
		orxBODY_PART *part = orxBody_GetNextPart(body, orxNULL);
		orxBody_SetPartSolid(part, orxTRUE);
		
		orxObject_SetParent(capturedBall, orxNULL); 		
		
		orxObject_SetCustomGravity(capturedBall, orxNULL);
		
		orxFLOAT x = orxMath_GetRandomFloat(kickoutMinimum.fX, kickoutMaximum.fX);
		orxFLOAT y = orxMath_GetRandomFloat(kickoutMinimum.fY, kickoutMaximum.fY);
		orxLOG("kick out at %f, %f", x, y);
		orxVECTOR kickOutVector = {x, y, 0}; //randomize it
		orxObject_SetSpeed(capturedBall, &kickOutVector);
		
		PlaySoundOn(activator, "SlingShotEffect");
		
	}
	
	capturedBall = orxNULL;
}

void saucerelement::DestroyCapturedBall(){
	ballIsCaptured = orxFALSE;
	
	if (capturedBall != orxNULL){
		orxObject_SetLifeTime(capturedBall, 0);
	}
	
	capturedBall = orxNULL;
}


orxOBJECT* saucerelement::GetCapturedBall(){
	return capturedBall;
}