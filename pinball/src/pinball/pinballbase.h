#ifndef PINBALLBASE_H
#define PINBALLBASE_H
#include <sstream>
#include "music.h"
#include "hidapi.h"
#include "usbreport.h"
#include "highscoreentry.h"
#include "indicators.h"

class PinballBase
{

#define COMMAND_PLAYER_TOGGLE_LED		    0x80
#define COMMAND_LAUNCH_TOGGLE_LED 			0x86
#define COMMAND_GET_BUTTON_STATES     		0x81
#define COMMAND_PLAYER_LED_ON     			0x82
#define COMMAND_PLAYER_LED_OFF     			0x83
#define COMMAND_LAUNCH_LED_ON 				0x84
#define COMMAND_LAUNCH_LED_OFF 				0x85

private:

	static void orxFASTCALL DancingLightsUpdateDispatcher(const orxCLOCK_INFO *_pstClockInfo, void *_pstContext);
	static void orxFASTCALL HalfSecondsUpdateDispatcher(const orxCLOCK_INFO *_pstClockInfo, void *_pstContext);
	static void orxFASTCALL SecondsUpdateDispatcher(const orxCLOCK_INFO *_pstClockInfo, void *_pstContext);
	static void orxFASTCALL ScoreUpdaterDispatcher(const orxCLOCK_INFO *_pstClockInfo, void *_pstContext);
	static orxSTATUS orxFASTCALL ShaderEventHandlerDispatcher(const orxEVENT *_pstEvent);
	static orxSTATUS orxFASTCALL CustomEventHandlerDispatcher(const orxEVENT *_pstEvent);
	static orxSTATUS orxFASTCALL TextureLoadingEventHandlerDispatcher(const orxEVENT *_pstEvent);
	static orxSTATUS orxFASTCALL EventHandlerDispatcher(const orxEVENT *_pstEvent);
	static orxSTATUS orxFASTCALL InputEventHandlerDispatcher(const orxEVENT *_pstEvent);
	static orxSTATUS orxFASTCALL AnimationEventHandlerDispatcher(const orxEVENT *_pstEvent);
	static orxSTATUS orxFASTCALL PhysicsEventHandlerDispatcher(const orxEVENT *_pstEvent);
	static void orxFASTCALL UpdateDispatcher(const orxCLOCK_INFO *_pstClockInfo, void *_pstContext);
	static orxBOOL orxFASTCALL SaveHighScoreToConfigFileCallbackDispatcher(const orxSTRING _zSectionName, const orxSTRING _zKeyName, const orxSTRING _zFileName, orxBOOL _bUseEncryption);

	int touchTotalFingersDown;
	int dancingLightIndex; //used by the controller light to know when to come on and off.

protected:
	PinballBase();
	static PinballBase* _instance;
	virtual ~PinballBase(){};

	orxFLOAT bumperStrength;
	orxVECTOR slingShotVector;
	orxFLOAT flipperRotationSteps;
	int plungerStrengthMultiplier;
	orxBOOL cheatMode;
	orxBOOL autoPlayMode;
	int secondsSinceTheLastTilt;
	int totalTiltPenalties;
	orxVECTOR lastAccel;
	
	/* USB Variables */ 
	hid_device *handle;
	
	highscoreitem highestScore;
	
	orxBOOL areControlsActive;

	virtual void orxFASTCALL DancingLightsUpdate(const orxCLOCK_INFO *_pstClockInfo, void *_pstContext);
	virtual void orxFASTCALL HalfSecondsUpdate(const orxCLOCK_INFO *_pstClockInfo, void *_pstContext);
	virtual void orxFASTCALL SecondsUpdate(const orxCLOCK_INFO *_pstClockInfo, void *_pstContext);
	virtual void orxFASTCALL ScoreUpdater(const orxCLOCK_INFO *_pstClockInfo, void *_pstContext);
	virtual orxSTATUS orxFASTCALL ShaderEventHandler(const orxEVENT *_pstEvent);
	virtual orxSTATUS orxFASTCALL CustomEventHandler(const orxEVENT *_pstEvent);
	virtual orxSTATUS orxFASTCALL TextureLoadingEventHandler(const orxEVENT *_pstEvent);
	virtual orxSTATUS orxFASTCALL EventHandler(const orxEVENT *_pstEvent);
	virtual orxSTATUS orxFASTCALL InputEventHandler(const orxEVENT *_pstEvent);
	virtual orxSTATUS orxFASTCALL AnimationEventHandler(const orxEVENT *_pstEvent);
	virtual orxSTATUS orxFASTCALL PhysicsEventHandler(const orxEVENT *_pstEvent);
	virtual void orxFASTCALL Update(const orxCLOCK_INFO *_pstClockInfo, void *_pstContext);
	virtual orxBOOL orxFASTCALL SaveHighScoreToConfigFileCallback(const orxSTRING _zSectionName, const orxSTRING _zKeyName, const orxSTRING _zFileName, orxBOOL _bUseEncryption);
	virtual void Tilt();
	virtual void SetCheatMode(orxBOOL onOff);
	
	orxBOOL IsEqualTo(const orxSTRING stringA, const orxSTRING stringB);
	
	/* USB Functions */
	orxBOOL InitUsbController(); //returns true if success
	//orxBOOL GetUsbButton();
	void UpdateUsbButtons();
	void LightPlayerButtonLed(orxBOOL onOff);
	void LightLaunchButtonLed(orxBOOL onOff);
	
	usbreport *report;
	orxBOOL launcherFlashOnOff;
	
public:
	static PinballBase* instance();
	static void create();	
	//static RasterBlaster* CreateRasterBlaster();
	virtual int DoSomething();

	orxVECTOR touchDownVector; //The location where a touch occurs.
	orxVECTOR touchUpVector; //The location where a touch is lifted.

	std::string scorePadding;

	static const int SLINGSHOT_VARIANCE = 200;
	static const int PLUNGER_SMACK_VARIANCE = 150;
	
	#ifdef _WIN32 
		static constexpr float DRAG_DISTANCE_MAX = 310;
	#elif __ANDROID__
		static const float DRAG_DISTANCE_MAX = 310;
	#else //LINUX MAC ANDROID
		static constexpr float DRAG_DISTANCE_MAX = 310;
	#endif // __orxDEBUG__	

	float TOUCH_WIDTH;
	float TOUCH_HEIGHT;
	float TOUCH_LEFT_X;
	float TOUCH_Y;
	float TOUCH_RIGHT_X;

	orxFLOAT nativeScreenWidth;
	orxFLOAT nativeScreenHeight;

	indicators *indicatorLights; 

	orxBOOL leftFlipperPressed;
	orxBOOL rightFlipperPressed;
	orxBOOL lastLeftFlipperPressedState;
	orxBOOL lastRightFlipperPressedState;
	orxBOOL launcherTrapOn;
	orxBOOL leftTrapOn;
	orxBOOL rightTrapOn;
	orxBOOL ballInPlay;
	orxBOOL launchMultiBalls;

	/* When all three are filled, launch multiball */
	orxBOOL rollOversFilled;
	orxBOOL targetGroupRightFilled;

	orxBOOL isAndroid;

	orxVIEWPORT *pstViewport;
	orxCAMERA 	*pstCamera;
	orxCLOCK    *pstDancingLightsClock;

	orxOBJECT *tableObject;
	orxOBJECT *tableSoundCentrePosition;
	orxOBJECT *tableMusicCentrePosition;
	orxOBJECT *ballObject;
	orxOBJECT *leftFlipperObject;
	orxOBJECT *rightFlipperObject;
	orxOBJECT *leftMiniFlipperObject;
	orxOBJECT *rightMiniFlipperObject;
	orxOBJECT *ballCatcherObject;
	orxOBJECT *scoreObject;
	orxOBJECT *ballCountObject;
	orxOBJECT *trapObject;
	orxOBJECT *trapLeftObject;
	orxOBJECT *trapRightObject;
	orxOBJECT *leftTouchObject;
	orxOBJECT *rightTouchObject;
	orxOBJECT *swipeTouchObject;
	orxOBJECT *titleObject;
	orxOBJECT *loadingPageObject;
	orxOBJECT *loadingProgressObject;


	//elementgroup *targetGroupRight;
	elementgroup *targetGroupLeft;
	elementgroup *targetGroupMiddle;
	elementgroup *targetGroupRight;
	
	elementgroup *rolloverlightgroup;

	plunger *plung;

	music *musicSystem;

	std::vector<int> ballIncreases;

	std::vector<highscoreitem> highScores;

	int zeroOutScoreDelay;
	orxU64 score;
	int balls;
	int multiBalls;	
	
	/* Delayed function parameters */
	//const int trapMaxTime;
	static const int trapMaxTime = 10;	
	int trapTimer;
	int showTitleDelay;


	//static const int TEXTURE_TO_LOAD_MAX = 20; //34 textures to expect
	orxU32 texturesToLoadMax;
	//int texturesToLoadCount;
	orxBOOL endTextureLoad;	

	highscoreentry *highScoreEntry;
	
	virtual void Init();


	void ProcessBallAndBumperCollision(orxOBJECT *ballObject, orxOBJECT *bumperObject, orxVECTOR hitVector);
	virtual void ProcessBallAndLeftSlingShotCollision(orxOBJECT *ballObject, orxOBJECT *leftSlingShotObject);
	virtual void ProcessBallAndRightSlingShotCollision(orxOBJECT *ballObject, orxOBJECT *rightSlingShotObject);
	//virtual void ProcessBallAndTargetTopCollision(orxOBJECT *ballObject, orxOBJECT *targetLeftObject);
	//virtual void ProcessBallAndTargetRightCollision(orxOBJECT *ballObject, orxOBJECT *targetRightObject);
	virtual void ProcessBallAndTargetCollision(orxOBJECT *ballObject, orxOBJECT *targetObject);
	void ProcessBallAndRolloverLightCollision(orxOBJECT *targetRightObject);
	void ProcessBallAndDirectionRolloverCollision(orxOBJECT *directionRollOverObject, orxSTRING collidedBodyPart);
	virtual void PrintScore();
	virtual void PrintScore(orxU64 overriddenScore); //use to not use "score" by default.

	virtual void ProcessAndroidAccelerometer();

	void CreateBall();
	void CreateBall(orxBOOL supressBallCountReduction);
	virtual void DestroyBall(orxOBJECT *ballObject);
	virtual void DestroyBallAndCreateNewBall(orxOBJECT *ballObject);
	void LaunchBall(); //launch any ball found above the plunger
	void PlungerRestore();
	virtual void AddToScore(int points);
	virtual void AddToScoreAndUpdate(int points);
	virtual void ProcessEventsBasedOnScore();
	int GetMultiplier(); //whats the current lit multiplier on the beanpole? values: 1-6
	void IncreaseMultiplier();
	virtual void TurnOffAllMultiplierLights();
	virtual void TurnOffAllTableLights();
	void SetLauncherTrap(orxBOOL close); //true to close
	void SetLeftTrap(orxBOOL close); //true to close
	void SetRightTrap(orxBOOL close); //true to close
	void FlashLeftFlukeLight();
	void FlashRightFlukeLight();
	void DecreaseBallCount();
	void IncreaseBallCount(int scoreBonusMaker); //save the score bonus and increase ball count so it doesn't happen twice.
	void PrintBallCount();
	void StrikeLeftFlipper();
	void StrikeRightFlipper();
	void SetLeftFlipper(orxBOOL pressed);
	void SetRightFlipper(orxBOOL pressed);
	virtual void StartNewGame();
	void SetupTouchZonesValues(orxFLOAT deviceWidth, orxFLOAT deviceHeight);
	void SetTouchHighlightPositions(orxFLOAT deviceWidth, orxFLOAT deviceHeight);
	void SetProgressPercent(orxFLOAT percent);

	void CreateSpanglesAtObject(orxOBJECT *object, orxSTRING particleNameFromConfig);
	void ShowTitleSlide(int slideNumberToShow);
	void ShowTitle();
	void HideTitle();
	
	void PlaySoundOn(orxOBJECT *object, orxSTRING soundFromConfig);
	void PlaySoundIfNotAlreadyPlaying(orxOBJECT *object, orxSTRING soundFromConfig);
	void PlaySoundOff(orxOBJECT *object, orxSTRING soundFromConfig);
	orxFLOAT GetSoundVolumePercent(orxOBJECT *object);
	void SetSoundPitchAndVolumePercent(orxOBJECT *object, orxFLOAT percent);

	orxBOOL IsObjectInitialised(orxOBJECT *object);

	highscoreitem GetSavedHighScore();
	std::vector<highscoreitem> GetSavedHighScores();
	void LoadSavedHighScores();
	void SaveHighScoreToConfig();
	void SaveHighScoresToConfig();
	void TryUpdateHighscore();

	void SetFrustumToWhateverDisplaySizeCurrentlyIs();

	orxBOOL IsWithin(float x, float y, float x1, float y1, float x2, float y2); //is x,y within the x1,y1 x2, y2 box
	orxBOOL IsGameOver();
	orxBOOL IsHighScoreEntryMode();
	orxBOOL IsTitleVisible();
	orxBOOL HasTableBeenTilted();
	orxBOOL AreControlsActive(); //tests for HasTableBeenTilted and areControlsActive
	void ActivateControls();
	void DisactivateControls();

	orxVECTOR GetAdjustedTouchCoords(orxVECTOR touchInVector);
	orxVECTOR ScreenToWorldCoords(orxVECTOR inVector);
	orxVECTOR WorldToScreenCoords(orxVECTOR inVector);
	orxVECTOR FlipVector(orxVECTOR);
	orxVECTOR VaryVector(orxVECTOR vector, int maxVariance); //vary fx and fy by x units. -x/2 to +x/2
	orxVECTOR VaryVectorY(orxVECTOR vector, int maxVariance); //vary fx by x units. -x/2 to +x/2

	orxOBJECT* GetABallObjectAtThePlunger();
	orxOBJECT* GetABallObjectIntheChannel();

	orxFLOAT GetSuitableFrustumWidthFromCurrentDevice();
	orxFLOAT GetSuitableFrustumHeightFromCurrentDevice();
	void DeterminePlatform();


};

#endif // PINBALLBASE_H
