#ifndef RELOADBAR_H
#define RELOADBAR_H

#include "pinballbase.h"
#include "spinnerelement.h"
//#include "indicators.h"
#include "achievements.h"
#include "bumpertargetelement.h"
#include "saucerelement.h"
#include "textdisplay.h"
#include "finaltarget.h"
#include "beers.h"
#include "highscoreentry.h"


class ReloadBar : public PinballBase 
{

	
public:
	static void create();
	virtual void Init();
	
	virtual void PrintScore();	
	virtual void PrintScore(orxU64 overriddenScore); //use to not use "score" by default.
	
	virtual void TurnOffAllMultiplierLights();
	virtual void TurnOffAllTableLights();
	virtual void StartNewGame();
	virtual void DestroyBall(orxOBJECT *ballObject);
	virtual void DestroyBallAndCreateNewBall(orxOBJECT *ballObject); //need override to ensure the correct indicators are set on new ball

	virtual void AddToScore(int points);
	
	orxBOOL targetGroupLeftFilled;
	orxBOOL targetGroupMiddleFilled;
	orxBOOL targetGroupRightFilled;
	
	orxU64 displayedScore;
	
	spinnerelement *spinner;
	spinnerelement *spinner2;
	
	saucerelement *saucer1;
	saucerelement *saucer2;

	elementgroup *bumperTargetGroup;

	textdisplay *textdisplayer;

	//indicators *indicatorLights; 
	achievements *achievementsRegister;
	
	finaltarget *finalTarget;
	//bumpertargetelement *finalBumperTarget;
	
	beers *beersModule;
	
	static const int SLINGSHOT_VARIANCE = 400;

	void MakeElementAChildOfTheTable(element *e);


protected:
	ReloadBar();
	virtual ~ReloadBar();
	
	virtual orxSTATUS orxFASTCALL TextureLoadingEventHandler(const orxEVENT *_pstEvent);
	virtual orxSTATUS orxFASTCALL PhysicsEventHandler(const orxEVENT *_pstEvent);
	virtual orxSTATUS orxFASTCALL AnimationEventHandler(const orxEVENT *_pstEvent);
	virtual orxSTATUS orxFASTCALL InputEventHandler(const orxEVENT *_pstEvent);
	virtual orxSTATUS orxFASTCALL ShaderEventHandler(const orxEVENT *_pstEvent);
	virtual orxSTATUS orxFASTCALL CustomEventHandler(const orxEVENT *_pstEvent);
	virtual void orxFASTCALL SecondsUpdate(const orxCLOCK_INFO *_pstClockInfo, void *_pstContext);
	virtual void orxFASTCALL ScoreUpdater(const orxCLOCK_INFO *_pstClockInfo, void *_pstContext);
	virtual void orxFASTCALL Update(const orxCLOCK_INFO *_pstClockInfo, void *_pstContext);

	virtual void ProcessEventsBasedOnScore();
	virtual void ProcessAndroidAccelerometer();

	virtual void ProcessBallAndLeftSlingShotCollision(orxOBJECT *ballObject, orxOBJECT *leftSlingShotObject);
	virtual void ProcessBallAndRightSlingShotCollision(orxOBJECT *ballObject, orxOBJECT *rightSlingShotObject);
	
	virtual void ProcessBallAndBumperTargetCollision(orxOBJECT *bumperTargetObject);
	virtual void ProcessBallAndSaucerCollision(orxOBJECT *ballObject, orxOBJECT *saucerObject);
	
	virtual void ProcessBallAndFinalTargetCollision(orxOBJECT *ballObject, orxOBJECT *finalTargetObject);

	virtual void FlashTiltWarning();
	virtual void Tilt();
	
private:

	void LaunchMultiBall(); //launch like in bean farmer style

	orxBOOL winIsImminent;
	orxBOOL hasWon;

	orxBOOL IsElegibleToWinBeer(); //is the user elegible to win a beer?

	void AdminClearNoticesContinueGame();

	int CalcTrapTimer(int currentTimer, int maxTimer, int multiplier);

	void TryUsbConnect();

	int autoPlayModeDelay;
};

#endif // RELOADBAR_H
