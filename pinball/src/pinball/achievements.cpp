#include "orx.h"
#include "achievements.h"

achievements::achievements(indicators *i)
{
	indicatorz = i;
	this->ClearAllAchievements();
	
	orxMath_InitRandom((orxS32)orxSystem_GetRealTime());
	
	cocktailPanelObject = orxObject_CreateFromConfig("CocktailPanelObject");
	HideCocktailPanel();
}

achievements::~achievements()
{
}

orxBOOL achievements::AllIsAchieved(){
	return leftTargetAchieved && midTargetAchieved && rightTargetAchieved && leftSaucerAchieved && rightSaucerAchieved && rollOversAchieved
	&& MultiplierMaxed();
}

orxBOOL achievements::AllAchievedIsImminent(){
	int achievementCount = 0;
	achievementCount += (int)leftTargetAchieved;
	achievementCount += (int)midTargetAchieved;
	achievementCount += (int)rightTargetAchieved;
	achievementCount += (int)leftSaucerAchieved;
	achievementCount += (int)rightSaucerAchieved;
	achievementCount += (int)rollOversAchieved;
	
	if (MultiplierAlmostMaxed() && achievementCount == IMMINENT_ACHIEVEMENT_COUNT){
		return orxTRUE;
	}
	
	if (MultiplierMaxed() && achievementCount == IMMINENT_ACHIEVEMENT_COUNT - 1 ){
		return orxTRUE;
	}
	
	return orxFALSE;
}

void achievements::LoseSomeAchievements(){
	
	indicatorz->DecreaseMultiplier();
	
	int choice = orxMath_GetRandomU32 (0, 6); 
	
	switch (choice) {
		case 0:
			SetLeftTargetAchieved(orxFALSE);
			break;
		case 1:
			SetMidTargetAchieved(orxFALSE);
			break;
		case 2:
			SetRightTargetAchieved(orxFALSE);
			break;
		case 3:
			SetLeftSaucerAchieved(orxFALSE);
			break;
		case 4:
			SetRightSaucerAchieved(orxFALSE);
			break;
		case 5:
			SetRollOversAchieved(orxFALSE);
			break;

	}	
}

orxBOOL achievements::BothIndicatorsMaxed(){
	if (IndicatorMaxed() && MultiplierMaxed()) {
		return orxTRUE;
	}
	
	return orxFALSE;
}

orxBOOL achievements::IndicatorMaxed(){
	if (indicatorz->GetIndicatorLevel() == 10){
		return orxTRUE;
	}
	
	return orxFALSE;
}

orxBOOL achievements::MultiplierMaxed(){
	if (indicatorz->GetMultiplier() == 10){
		return orxTRUE;
	}
	
	return orxFALSE;
}

orxBOOL achievements::MultiplierAlmostMaxed(){
	if (indicatorz->GetMultiplier() == 8){
		return orxTRUE;
	}
	
	return orxFALSE;
}

void achievements::SetLeftTargetAchieved(orxBOOL yesNo){
	leftTargetAchieved = yesNo;			
	indicatorz->LightE(yesNo);
}

void achievements::SetMidTargetAchieved(orxBOOL yesNo){
	midTargetAchieved = yesNo;
	indicatorz->LightO(yesNo);
}

void achievements::SetRightTargetAchieved(orxBOOL yesNo){
	rightTargetAchieved = yesNo;			
	indicatorz->LightA(yesNo);
}

void achievements::SetLeftSaucerAchieved(orxBOOL yesNo){
	leftSaucerAchieved = yesNo;
	indicatorz->LightR(yesNo);
}

void achievements::SetRightSaucerAchieved(orxBOOL yesNo){
	rightSaucerAchieved = yesNo;
	indicatorz->LightD(yesNo);
}			

void achievements::SetRollOversAchieved(orxBOOL yesNo){
	rollOversAchieved = yesNo;
	indicatorz->LightL(yesNo);
}

void achievements::ClearAllAchievements(){
	this->SetLeftTargetAchieved(orxFALSE);
	this->SetMidTargetAchieved(orxFALSE);
	this->SetRightTargetAchieved(orxFALSE);
	this->SetLeftSaucerAchieved(orxFALSE);
	this->SetRightSaucerAchieved(orxFALSE);
	this->SetRollOversAchieved(orxFALSE);
}

void achievements::HideCocktailPanel(){
	//orxVECTOR hiddenPosition = {xHidden, 0, -0.2};
	//orxObject_SetPosition(cocktailPanelObject, &hiddenPosition);
	orxObject_EnableRecursive(cocktailPanelObject, orxFALSE);
}

void achievements::ShowCocktailPanel(){
	//orxVECTOR hiddenPosition = {xVisible, 0, -0.2};
	//orxObject_SetPosition(cocktailPanelObject, &hiddenPosition);
	orxObject_EnableRecursive(cocktailPanelObject, orxTRUE);
}

orxBOOL achievements::IsCocktailPanelVisible(){
	//orxVECTOR position = {0,0,0};
	//orxObject_GetPosition(cocktailPanelObject, &position);
	
	//return position.fX == xVisible;
	return orxObject_IsEnabled(cocktailPanelObject);
	
}