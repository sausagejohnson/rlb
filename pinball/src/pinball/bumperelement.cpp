#include "orx.h"
#include "element.h"
#include "bumperelement.h"

bumperelement::bumperelement(int points) : element(points, (orxCHAR*)"BumperObject", orxNULL) {
	activatorHitAnimationName = (orxCHAR*)"BumperHitAnim";
	lightOnAnimationName = orxNULL;
}

bumperelement::~bumperelement() {

}
	orxOBJECT GetActivator(); 
	orxOBJECT GetLight(); 
