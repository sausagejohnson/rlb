#include "orx.h"
#include "element.h"
#include "targetelement.h"

targetelement::targetelement(int points, orxFLOAT rotateTarget) : element(points, (orxCHAR*)"TargetObject", (orxCHAR*)"TargetLightObject", rotateTarget) {
//targetelement::targetelement(int points, orxFLOAT rotateTarget) : element(points, (orxCHAR*)"TargetObject", (orxCHAR*)"TargetLightObject") {
	activatorHitAnimationName = (orxCHAR*)"TargetHitAnim";
	lightOnAnimationName = (orxCHAR*)"TargetLightOnAnim";
	lightOffAnimationName = (orxCHAR*)"TargetLightOffAnim";
	lightFlashAnimationName = (orxCHAR*)"TargetLightFlashAnim";
}

targetelement::~targetelement()
{
}

void targetelement::RunRules() {
	//orxLOG("----------------targetelement RunRules");
	element::RunRules();
}



