#include "orx.h"
#include "music.h"

music::music(orxOBJECT *parentObject)
{
	songNames.push_back("Flibba");
	songNames.push_back("DigitalCowboys");
	songNames.push_back("Phreakin");
	songNames.push_back("Nice");
	songNames.push_back("PromoOnly");
	songNames.push_back("Relapse");
	
	musicObject = parentObject;
	
	orxMath_InitRandom((orxS32)orxSystem_GetRealTime());
	
	trackNumber = orxMath_GetRandomU32(0, 5);
}

music::~music()
{
}

void music::Stop()
{
	orxSOUND *currentSound = orxObject_GetLastAddedSound(musicObject);
	if (currentSound != orxNULL){
		orxObject_RemoveSound(musicObject, orxSound_GetName(currentSound)); 
	}
}

void music::Play()
{
	Stop();
	
	trackNumber++;
	if (trackNumber >= songNames.size()){
		trackNumber = 0;
	}
	//orxLOG("Track: %d", trackNumber);
	
	orxObject_AddSound(musicObject, songNames[trackNumber]);
}

