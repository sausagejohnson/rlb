#ifndef TARGETELEMENT_H
#define TARGETELEMENT_H

class targetelement : public element
{
public:
	targetelement(int points, orxFLOAT rotateTarget);
	~targetelement();
	
	void RunRules();
};

#endif // TARGETELEMENT_H
