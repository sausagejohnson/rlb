#include "orx.h"
#include "element.h"
#include "bumpertargetelement.h"

bumpertargetelement::bumpertargetelement(int points, orxFLOAT rotateTarget) : element(points, (orxCHAR*)"BumperTargetObject", orxNULL, rotateTarget) {
	activatorHitAnimationName = (orxCHAR*)"BumperTargetLightBlink";//BumperTargetInToOut
	activatorAnimationName = (orxCHAR*)"BumperTargetLightOff";//BumperTargetLightOff
	activatorFlashAnimationName = (orxCHAR*)"BumperTargetLightBlink";//BumperTargetLightBlink
}


bumpertargetelement::~bumpertargetelement()
{
}

