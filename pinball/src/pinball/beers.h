#ifndef BEERS_H
#define BEERS_H
#include <vector>
#include <ctime>

class beer
{
public:
	beer(int hour, int minute);
	~beer();
	
	void SetAsWon(); 
	orxBOOL HasBeenWon(); //if true, cannot be won again.
	//Note, if the values are tampered with, they should reload.
	//If the time goes outside of the zone, and there are some beers not reset, they should reset.
	
	int Hour();
	int Minute();
	
private:
	int _hour; //hour of the beer to win
	int _minute; //minute of the beer to win
	int _hasBeenWon; //has this beer been won already?
};

/* ------------------------------ */

class beers
{
public:
	beers();
	~beers();
	
	orxBOOL HasJustWonABeer(); //flags if a beer has just been won. Main routine should pause game, and upon returning, clear this value.

	void ResetAllWins(); //so that the wins can begin again the next day.
	void HideWinPanel();
	void ShowWinPanel();
	orxBOOL IsWinPanelVisible(); //if it is, no extra win should be given.
	
private:
	orxOBJECT *beerPanelObject;
	int _startHour;
	int _endHour;
	int _totalBeersToWin;
	orxBOOL _hasJustWonABeer;

//	int xVisible;
//	int xHidden;

	std::vector<beer*> beersAvailable;
	std::tm GetCurrentLocalTime();
	std::time_t	ConvertToTime(orxFLOAT selectedValue);
	std::time_t	AddMinutesToStartTime(int minutes);
	int GetHourFromTimeValue(std::time_t time);	
	

};


#endif // BEERS_H
