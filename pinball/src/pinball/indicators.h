#ifndef INDICATORS_H
#define INDICATORS_H

class indicators
{
public:
	indicators();
	~indicators();
	
	void Flash1();
	void Flash5();
	void Flash10();
	void Flash50();
	void Flash100();
	
	void LightR(orxBOOL enable); //true for on, false for off
	void LightE(orxBOOL enable); //true for on, false for off
	void LightL(orxBOOL enable); //true for on, false for off
	void LightO(orxBOOL enable); //true for on, false for off
	void LightA(orxBOOL enable); //true for on, false for off
	void LightD(orxBOOL enable); //true for on, false for off
	void LightApos(orxBOOL enable); //true for on, false for off
	
	orxBOOL IsRLit();
	orxBOOL IsELit();
	orxBOOL IsLLit();
	orxBOOL IsOLit();
	orxBOOL IsALit();
	orxBOOL IsDLit();
	orxBOOL IsAposLit();
	
	void TurnOnIndicatorLight(); // how many lights to light up from the bottom
	void TurnOnIndicatorLightAndFlashHighest(); // how many lights to light up from the bottom
	
	void SetIndicatorLevel(int value); //1 to 10
	int GetIndicatorLevel(); //1 to 10
	void IncreaseIndicator();
	void ClearIndicators();
	
	int indicatorLevel;
	
	//void FlashR();
	//void FlashB();
	
	//void FlashExtraBall();
	//void FlashTilt();

	void TurnAllMultiplierLightsOff();
	void TurnAllIndicatorLightsOff();
	void ClearBonusAndLights(); //clears the bonus value before calling TurnAllBonusLightsOff()
	void TurnAllLightsOff();
	void ResetAll(); //clears all lights and bonus completely.
	void ResetAllExceptMultiplier();
	void TurnReloadLightsOff();
	
	void SetExtraBallPending(orxBOOL onOff);

	void SetMultiplier(int number);
	void IncreaseMultiplier();
	void DecreaseMultiplier();
	int GetMultiplier();
	
	void AddToBonus(int bonusIncrease);
	
	void SnapshotBonus(); //copy bonus value. Used at gameover time.
	
	
	void ParentTo(orxOBJECT *parent); //set all objects in the indicator to be children of passed in object.
	
	int bonus; //stored as the actual bonus value: 5000, 15000, 200000 etc
	int bonusSnapshotAtGameOver; //copy of the last bonus value at game over. Used to calc diff from bonus to animate the multiplier down.
	
	//orxBOOL IsROn();
	//orxBOOL IsBOn();
	orxBOOL IsExtraBallPending();

private:

	orxOBJECT *object1x;
	orxOBJECT *object2x;
	orxOBJECT *object4x;
	orxOBJECT *object8x;
	orxOBJECT *object10x;
	
	orxOBJECT *object1;
	orxOBJECT *object2;
	orxOBJECT *object3;
	orxOBJECT *object4;
	orxOBJECT *object5;
	orxOBJECT *object6;
	orxOBJECT *object7;
	orxOBJECT *object8;
	orxOBJECT *object9;
	orxOBJECT *object10;
	
	orxOBJECT *objectR;
	orxOBJECT *objectE;
	orxOBJECT *objectL;
	orxOBJECT *objectO;
	orxOBJECT *objectA;
	orxOBJECT *objectD;
	orxOBJECT *objectApos;
	
	//orxOBJECT *objectR;
	//orxOBJECT *objectB;
	
	//orxOBJECT *objectExtraBall;
	//orxOBJECT *objectTilt;
	
	int multiplierValue;
	orxBOOL extraBallPending;

};

#endif // INDICATORS_H
