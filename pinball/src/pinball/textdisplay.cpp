#include "orx.h"
#include "textdisplay.h"
#include <string>
#include <sstream>
textdisplay::textdisplay(orxFLOAT x, orxFLOAT y)
{
	textDisplayObject = orxObject_CreateFromConfig("LCDObject");
	SaveZPosition();
	
	orxVECTOR position = { x, y, initialZ };
	orxObject_SetPosition(textDisplayObject, &position);
	
	scrollIndex	 = 0;
	whatHappensAfterScrollEnds = 0; 

	scrollUpdateClock = orxClock_Create(orx2F(0.3f), orxCLOCK_TYPE_USER);
	
	orxClock_Register(scrollUpdateClock, ScrollUpdateDispatcher, this, orxMODULE_ID_MAIN, orxCLOCK_PRIORITY_NORMAL);
	orxClock_Pause(scrollUpdateClock);
	
}

textdisplay::~textdisplay()
{
}

//maxiumum 11 chars only
void textdisplay::SetText(orxSTRING text){
	//orxLOG("SetText()");
	textToDisplay = orxString_Duplicate(text);
}

void textdisplay::ShowText(){
	//orxLOG("ShowText()");
	orxObject_RemoveTimeLineTrack(textDisplayObject, "LCDClearInAFewSecondsTrack");
	orxObject_RemoveTimeLineTrack(textDisplayObject, "LCDFlashTrack");
	orxObject_SetTextString(textDisplayObject, textToDisplay);
	orxObject_AddTimeLineTrack(textDisplayObject, "LCDClearInAFewSecondsTrack");
	
	orxClock_Pause(scrollUpdateClock);
}

void textdisplay::Flash(){
	//StopFlashing();
	orxLOG("Flash()");
	orxObject_RemoveTimeLineTrack(textDisplayObject, "LCDClearInAFewSecondsTrack");
	orxObject_RemoveTimeLineTrack(textDisplayObject, "LCDFlashTrack");
	orxObject_AddTimeLineTrack(textDisplayObject, "LCDFlashTrack");
	//orxObject_RemoveFX(textDisplayObject, "LCDRestore");
	//orxObject_AddFX(textDisplayObject, "LCDFlash");
}

//void textdisplay::StopFlashing(){
	//orxObject_RemoveFX(textDisplayObject, "LCDFlash");
	//orxObject_AddFX(textDisplayObject, "LCDRestore");
//}

void textdisplay::SaveZPosition(){
	orxVECTOR position;
	orxObject_GetPosition(textDisplayObject, &position);
	initialZ = position.fZ;
}

void textdisplay::ParentTo(orxOBJECT *parent){
	orxObject_SetParent(textDisplayObject, parent);
}

void textdisplay::SetScrollerText(orxSTRING text){
	
	std::stringstream aStringStream;
	aStringStream << "           " << text << "           ";
	std::string pieceOfString = aStringStream.str();
 
	orxSTRING total = (orxCHAR*)pieceOfString.c_str();
	scrollerTextToDisplay = orxString_Duplicate(total);
}

void textdisplay::Scroll(){
	orxClock_Restart(scrollUpdateClock);
	orxClock_Unpause(scrollUpdateClock);	
}

void textdisplay::BackToTextAfterScroll(){
	whatHappensAfterScrollEnds = 1;
}
void textdisplay::ScrollAgainAfterScroll(){
	whatHappensAfterScrollEnds = 2;
}
void textdisplay::BlankAfterScroll(){ 
	whatHappensAfterScrollEnds = 0;
}

void orxFASTCALL textdisplay::ScrollUpdateDispatcher(const orxCLOCK_INFO *_pstClockInfo, void *_pstContext)
{
	textdisplay *thisTextDisplay = (textdisplay*)_pstContext;
	thisTextDisplay->DoScroll(_pstClockInfo, _pstContext);
}

void textdisplay::ResetScrollToStart(){
	scrollIndex = 0;
}


void textdisplay::DoScroll(const orxCLOCK_INFO *_pstClockInfo, void *_pstContext)
{
	
	orxSTRING scrollSectionString;
	
	std::string pieceOfString = scrollerTextToDisplay;
	std::string snippet = pieceOfString.substr(scrollIndex, MAX_CHARS_WIDTH);
	
	scrollSectionString = (orxCHAR*)snippet.c_str();
	
	orxObject_SetTextString(textDisplayObject, scrollSectionString);
	
	int length = orxString_GetLength(scrollerTextToDisplay);
	
	scrollIndex++;
	if (scrollIndex >= length - MAX_CHARS_WIDTH +1){
		scrollIndex = 0;
		
		if (whatHappensAfterScrollEnds == 0){ //perhaps move out into it's own function
			orxClock_Pause(scrollUpdateClock);	
			SetText(" ");
			ShowText();
		} else if (whatHappensAfterScrollEnds == 1){
			orxClock_Pause(scrollUpdateClock);	
			ShowText();
		} else {
			//just do it again
		}
	}
}