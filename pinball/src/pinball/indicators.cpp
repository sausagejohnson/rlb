#include "orx.h"
#include "indicators.h"

indicators::indicators()
{
	object1x = orxObject_CreateFromConfig("1xObject");
	object2x = orxObject_CreateFromConfig("2xObject");
	object4x = orxObject_CreateFromConfig("4xObject");
	object8x = orxObject_CreateFromConfig("8xObject");
	object10x = orxObject_CreateFromConfig("10xObject");
	
	object1 = orxObject_CreateFromConfig("1Object");
	object2 = orxObject_CreateFromConfig("2Object");
	object3 = orxObject_CreateFromConfig("3Object");
	object4 = orxObject_CreateFromConfig("4Object");
	object5 = orxObject_CreateFromConfig("5Object");
	object6 = orxObject_CreateFromConfig("6Object");
	object7 = orxObject_CreateFromConfig("7Object");
	object8 = orxObject_CreateFromConfig("8Object");
	object9 = orxObject_CreateFromConfig("9Object");
	object10 = orxObject_CreateFromConfig("10Object");
	
	objectR = orxObject_CreateFromConfig("ReloadLightR");
	objectE = orxObject_CreateFromConfig("ReloadLightE");
	objectL = orxObject_CreateFromConfig("ReloadLightL");
	objectO = orxObject_CreateFromConfig("ReloadLightO");
	objectA = orxObject_CreateFromConfig("ReloadLightA");
	objectD = orxObject_CreateFromConfig("ReloadLightD");
	objectApos = orxObject_CreateFromConfig("ReloadLightApos");
	
	//objectR = orxObject_CreateFromConfig("RObject");
	//objectB = orxObject_CreateFromConfig("BObject");
	
	//objectExtraBall = orxObject_CreateFromConfig("ExtraBallObject");
	//objectTilt = orxObject_CreateFromConfig("TiltObject");
	
	//orxObject_Enable(objectTilt, orxFALSE);
	
	bonus = 0;
	bonusSnapshotAtGameOver = 0;
	
	multiplierValue = 1;
	SetMultiplier(multiplierValue);

	indicatorLevel = 0;

	extraBallPending = orxFALSE;

	TurnAllIndicatorLightsOff();
	TurnReloadLightsOff();
}


int indicators::GetIndicatorLevel(){
	return indicatorLevel;
}

void indicators::SetIndicatorLevel(int value){ //0 to 10, 0 is no lights
	if (value < 0 || value > 10){
		value = 1;
	}
	
	indicatorLevel = value;
}

void indicators::TurnOnIndicatorLightAndFlashHighest(){
	this->TurnOnIndicatorLight();
	
	int number = indicatorLevel;
	
	if (number == 1)
		orxObject_AddFX(object1, "IndicatorFlashFX");
	if (number == 2)
		orxObject_AddFX(object2, "IndicatorFlashFX");
	if (number == 3)
		orxObject_AddFX(object3, "IndicatorFlashFX");
	if (number == 4)
		orxObject_AddFX(object4, "IndicatorFlashFX");
	if (number == 5)
		orxObject_AddFX(object5, "IndicatorFlashFX");
	if (number == 6)
		orxObject_AddFX(object6, "IndicatorFlashFX");
	if (number == 7)
		orxObject_AddFX(object7, "IndicatorFlashFX");
	if (number == 8)
		orxObject_AddFX(object8, "IndicatorFlashFX");
	if (number == 9)
		orxObject_AddFX(object9, "IndicatorFlashFX");
	if (number == 10)
		orxObject_AddFX(object10, "IndicatorFlashFX");
}


void indicators::TurnOnIndicatorLight(){
	
	int number = indicatorLevel;
	
	if (number >= 1)
		orxObject_Enable(object1, orxTRUE);
	if (number >= 2)
		orxObject_Enable(object2, orxTRUE);
	if (number >= 3)
		orxObject_Enable(object3, orxTRUE);
	if (number >= 4)
		orxObject_Enable(object4, orxTRUE);
	if (number >= 5)
		orxObject_Enable(object5, orxTRUE);
	if (number >= 6)
		orxObject_Enable(object6, orxTRUE);
	if (number >= 7)
		orxObject_Enable(object7, orxTRUE);
	if (number >= 8)
		orxObject_Enable(object8, orxTRUE);
	if (number >= 9)
		orxObject_Enable(object9, orxTRUE);
	if (number >= 10)
		orxObject_Enable(object10, orxTRUE);
}


void indicators::IncreaseIndicator(){
	indicatorLevel++;
	if (indicatorLevel > 10){
		indicatorLevel = 10;
	} else {
		TurnOnIndicatorLightAndFlashHighest();
	}
}

void indicators::ClearIndicators(){
	indicatorLevel = 0;
	TurnAllIndicatorLightsOff();
}


void indicators::IncreaseMultiplier(){
	if (multiplierValue == 8)
		multiplierValue = 10;
	if (multiplierValue == 4)
		multiplierValue = 8;
	if (multiplierValue == 2)
		multiplierValue = 4;
	if (multiplierValue == 1)
		multiplierValue = 2;
	
	this->SetMultiplier(multiplierValue);
}


void indicators::DecreaseMultiplier(){
	if (multiplierValue == 2)
		multiplierValue = 1;
	if (multiplierValue == 4)
		multiplierValue = 2;
	if (multiplierValue == 8)
		multiplierValue = 4;
	if (multiplierValue == 10)
		multiplierValue = 8;
	
	this->SetMultiplier(multiplierValue);
	this->LightApos(orxFALSE);
}

void indicators::SetMultiplier(int number){
	
	//if (number < 1 || number > 5){
		if (multiplierValue == 10 && number == 10){
			//orxObject_Enable(objectExtraBall, orxTRUE);
			extraBallPending = orxTRUE;
		}
	//}
	
	TurnAllMultiplierLightsOff();
	

	
	if (number != multiplierValue){
		multiplierValue = number;
	}
	
	switch (number) {
		case 1:
			orxObject_Enable(object1x, orxTRUE);
			break;
		case 2:
			orxObject_Enable(object2x, orxTRUE);
			break;
		case 4:
			orxObject_Enable(object4x, orxTRUE);
			break;
		case 8:
			orxObject_Enable(object8x, orxTRUE);
			break;
		case 10:
			orxObject_Enable(object10x, orxTRUE);
			LightApos(orxTRUE);
			break;
	}
}

/* Valid values are 5000, 10000 or 15000, etc */
void indicators::AddToBonus(int bonusIncrease){
	//int previousValue = value;
	
	//TurnAllBonusLightsOff();
	
	
	bonus += bonusIncrease;
	//orxFLOAT small = orxFLOAT(bonus)/1000; ?? Not used??
	
	orxLOG("BONUS: %d", bonus);
	
}


void indicators::SnapshotBonus(){
	if (bonusSnapshotAtGameOver != 0){
		return; //can only be set once.
	}
	
	bonusSnapshotAtGameOver = bonus;
}


int indicators::GetMultiplier(){
	return multiplierValue;
}


void indicators::TurnAllMultiplierLightsOff(){
	orxObject_Enable(object1x, orxFALSE);
	orxObject_Enable(object2x, orxFALSE);
	orxObject_Enable(object4x, orxFALSE);
	orxObject_Enable(object8x, orxFALSE);
	orxObject_Enable(object10x, orxFALSE);
	//orxObject_Enable(objectExtraBall, orxFALSE);
}

void indicators::TurnAllIndicatorLightsOff(){
	orxObject_Enable(object1, orxFALSE);
	orxObject_Enable(object2, orxFALSE);
	orxObject_Enable(object3, orxFALSE);
	orxObject_Enable(object4, orxFALSE);
	orxObject_Enable(object5, orxFALSE);
	orxObject_Enable(object6, orxFALSE);
	orxObject_Enable(object7, orxFALSE);
	orxObject_Enable(object8, orxFALSE);
	orxObject_Enable(object9, orxFALSE);
	orxObject_Enable(object10, orxFALSE);
}

void indicators::TurnReloadLightsOff(){
	LightR(orxFALSE);
	LightE(orxFALSE);
	LightL(orxFALSE);
	LightO(orxFALSE);
	LightA(orxFALSE);
	LightD(orxFALSE);
	LightApos(orxFALSE);
}

orxBOOL indicators::IsRLit(){
	return orxObject_IsEnabled(objectR);
}

orxBOOL indicators::IsELit(){
	return orxObject_IsEnabled(objectE);
}

orxBOOL indicators::IsLLit(){
	return orxObject_IsEnabled(objectL);
}

orxBOOL indicators::IsOLit(){
	return orxObject_IsEnabled(objectO);
}

orxBOOL indicators::IsALit(){
	return orxObject_IsEnabled(objectA);
}

orxBOOL indicators::IsDLit(){
	return orxObject_IsEnabled(objectD);
}

orxBOOL indicators::IsAposLit(){
	return orxObject_IsEnabled(objectApos);
}


void indicators::ResetAll(){
	bonus = 0;
	bonusSnapshotAtGameOver = 0;
	TurnAllLightsOff();
	SetMultiplier(1);
}


void indicators::ResetAllExceptMultiplier(){
	bonus = 0;
	bonusSnapshotAtGameOver = 0;
	TurnAllIndicatorLightsOff();
	//TurnRBLightsOff();

	extraBallPending = orxFALSE;
}


void indicators::ClearBonusAndLights(){
	bonus = 0;
	bonusSnapshotAtGameOver = 0;
	TurnAllIndicatorLightsOff();
}

void indicators::LightR(orxBOOL enable){
	orxObject_Enable(objectR, enable);
}
void indicators::LightE(orxBOOL enable){
	orxObject_Enable(objectE, enable);
}
void indicators::LightL(orxBOOL enable){
	orxObject_Enable(objectL, enable);
}
void indicators::LightO(orxBOOL enable){
	orxObject_Enable(objectO, enable);
}
void indicators::LightA(orxBOOL enable){
	orxObject_Enable(objectA, enable);
}
void indicators::LightD(orxBOOL enable){
	orxObject_Enable(objectD, enable);
}
void indicators::LightApos(orxBOOL enable){
	orxObject_Enable(objectApos, enable);
}


void indicators::TurnAllLightsOff(){
	TurnAllIndicatorLightsOff();
	TurnAllMultiplierLightsOff();
	TurnReloadLightsOff();
	
	extraBallPending = orxFALSE;
}

void indicators::SetExtraBallPending(orxBOOL onOff){
	extraBallPending = onOff;
}

orxBOOL indicators::IsExtraBallPending(){
	return extraBallPending;
}

void indicators::ParentTo(orxOBJECT *parent){
	orxObject_SetParent(object1x, parent);
	orxObject_SetParent(object2x, parent);
	orxObject_SetParent(object4x, parent);
	orxObject_SetParent(object8x, parent);
	orxObject_SetParent(object10x, parent);
	orxObject_SetParent(object1, parent);
	orxObject_SetParent(object2, parent);
	orxObject_SetParent(object3, parent);
	orxObject_SetParent(object4, parent);
	orxObject_SetParent(object5, parent);
	orxObject_SetParent(object6, parent);
	orxObject_SetParent(object7, parent);
	orxObject_SetParent(object8, parent);
	orxObject_SetParent(object9, parent);
	orxObject_SetParent(object10, parent);
	
	orxObject_SetParent(objectR, parent);
	orxObject_SetParent(objectE, parent);
	orxObject_SetParent(objectL, parent);
	orxObject_SetParent(objectO, parent);
	orxObject_SetParent(objectA, parent);
	orxObject_SetParent(objectD, parent);
	orxObject_SetParent(objectApos, parent);
	//orxObject_SetParent(objectExtraBall, parent);
	//orxObject_SetParent(objectTilt, parent);
}


indicators::~indicators()
{
}

