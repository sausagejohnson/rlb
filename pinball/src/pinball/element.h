#ifndef ELEMENT_H
#define ELEMENT_H

#include "elementgroup.h"

class element {

protected:
	int points;
	int pointsCollected;
	orxOBJECT *activator;
	orxOBJECT *light;
	orxSTRING activatorAnimationName; //usually not needed unless activator is to be restored manually.
	orxSTRING activatorHitAnimationName;
	orxSTRING activatorFlashAnimationName;
	orxSTRING activatorFXName; //used with or as an alternative to activatorAnimationName 
	orxSTRING lightFXName; //used with or as an alternative to activatorAnimationName 
	orxSTRING lightOnAnimationName;
	orxSTRING lightOffAnimationName;
	orxSTRING lightFlashAnimationName;
	orxBOOL LightExists();
	void SaveZPositions();
	orxFLOAT initialActivatorZ;
	orxFLOAT initialLightZ;
private:
	void SetLight(orxBOOL);
	elementgroup *parentElementGroup;
	orxBOOL ActivatorExists();
	orxBOOL ActivatorFXExists();
	void CreateSpanglesAtObject(orxOBJECT *object, orxSTRING particleNameFromConfig); //duplicated from pinball.h
public:
	element(int p, orxSTRING activatorNameFromConfig, orxSTRING lightNameFromConfig);
	element(int p, orxSTRING activatorNameFromConfig, orxSTRING lightNameFromConfig, orxSTRING fxNameFromConfig);
	element(int p, orxSTRING activatorNameFromConfig, orxSTRING lightNameFromConfig, orxFLOAT rotateElement);
	~element();
	
	int RegisterHit();
	int RegisterHitAsFX(); //used with or as an alternative to activatorAnimationName 
	void SetActivatorPosition(int x, int y);
	void SetLightPosition(int x, int y);

	void SetActivatorFX(orxSTRING fxName);
	void SetLightFX(orxSTRING fxName);

	void virtual ProcessHit(); //orxSTRING activatorHitAnimationName, orxSTRING lightOnAnimationName
	void virtual ProcessFXHit(); //orxSTRING fXHitName
	void virtual RunRules(); //process rules and return some points
	void SetElementParentGroup(elementgroup *group);
	orxBOOL IsLit();
	orxBOOL IsActivatorOn(); //For checking activators on or lit that stay lit.
	void TurnOffLight();
	void TurnOnLight();
	void TurnOnActivator();
	void TurnOnFXActivator(); //will need an off for looping FX
	void TurnOffActivator();
	int CollectAndClearPoints();
	void FlashLight();
	void FlashActivator();
	void CreateSpanglesOnActivator(orxSTRING particleNameFromConfig); 
	void CreateSpanglesOnLight(orxSTRING particleNameFromConfig); 
	orxOBJECT* GetActivator(); 
	orxOBJECT* GetLight(); 
};

#endif // ELEMENT_H
