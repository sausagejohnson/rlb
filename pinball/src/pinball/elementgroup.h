#ifndef ELEMENTGROUP_H
#define ELEMENTGROUP_H

#include <vector>

class element;

class elementgroup
{
private:
	std::vector<element*> elements;
	orxSTRING eventName;
	int pointsWhenGroupFilled;

public:
	elementgroup();
	elementgroup(int pointsWhenFilled);
	~elementgroup();

	void Add(element *e); //add element and return new count
	int RunGroupRules();
	size_t LightsOnCount();
	void TurnAllLightsOff();
	void FlashAllLights();
	void FlashAllActivators();
	void TurnActivatorOnAt(int index);
	void TurnLightOnAt(int index);
	size_t ActivatorsOnCount();
	void TurnAllActivatorsOff();
	void ShiftActivatorsRight();
	void ShiftLightsRight();
	void DoSpanglesOnActivators();
	void DoSpanglesOnLights();
	void SetEventName(orxSTRING eventName);
};

#endif // ELEMENTGROUP_H
