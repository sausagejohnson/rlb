#include "orx.h"
#include "highscoreentry.h"
#include <string>

highscoreentry::highscoreentry()
{
	highScoreEntryPanelObject = orxObject_CreateFromConfig("HighScoreEntryObject");
	highScoreInitials1Object = orxObject_CreateFromConfig("HighScoreInitials1Object");
	highScoreInitials2Object = orxObject_CreateFromConfig("HighScoreInitials2Object");
	highScoreInitials3Object = orxObject_CreateFromConfig("HighScoreInitials3Object");
	highScoreInitialIndicatorObject = orxObject_CreateFromConfig("TitleHighScoreInitialIndicator");

	orxObject_SetParent(highScoreInitials1Object, highScoreEntryPanelObject);
	orxObject_SetOwner(highScoreInitials1Object, highScoreEntryPanelObject);
	orxObject_SetParent(highScoreInitials2Object, highScoreEntryPanelObject);
	orxObject_SetOwner(highScoreInitials2Object, highScoreEntryPanelObject);
	orxObject_SetParent(highScoreInitials3Object, highScoreEntryPanelObject);
	orxObject_SetOwner(highScoreInitials3Object, highScoreEntryPanelObject);
	orxObject_SetOwner(highScoreInitialIndicatorObject, highScoreEntryPanelObject);

	alphabet = (orxCHAR*)"ABCDEFGHIJKLMNOPQRSTUVWXYZ-";
	
	initial1 = '-';
	initial2 = '-';
	initial3 = '-';
	
	//name[4] = {};
	
	Hide();
}

highscoreentry::~highscoreentry()
{
}

orxCHAR highscoreentry::GetCharAtIndex(int pos){
	//orxCHAR buffer[2]; 
	orxCHAR buffer; 
    buffer = alphabet[alphabetIndex];
	//orxString_NPrint(buffer, 2, "%s", alphabet + alphabetIndex); 
	
	return buffer;
}


void highscoreentry::Hide(){
	orxObject_EnableRecursive(highScoreEntryPanelObject, orxFALSE);
}

void highscoreentry::Show(){
	initialIndex = 0;
	alphabetIndex = 0;
	orxObject_SetTextString(highScoreInitials1Object, "A"); 
	orxObject_SetTextString(highScoreInitials2Object, "A"); 
	orxObject_SetTextString(highScoreInitials3Object, "A"); 
	ResetIndicator();
	orxObject_EnableRecursive(highScoreEntryPanelObject, orxTRUE);
	
	initial1 = 'A';
	initial2 = 'A';
	initial3 = 'A';
	
//	orxString_NPrint(initial1, sizeof(initial1)-1, "%s", "A");
//	orxString_NPrint(initial2, sizeof(initial1)-1, "%s", "A");
//	orxString_NPrint(initial3, sizeof(initial1)-1, "%s", "A");
	
	ClearFX(highScoreInitials3Object);
	ClearFX(highScoreInitialIndicatorObject);
	orxObject_AddFX(highScoreInitials1Object, "HighScoreLetterSelectedPulseFXSlot");	
	orxObject_AddFX(highScoreInitialIndicatorObject, "HighScoreLetterSelectedPulseFXSlot");	
}

orxBOOL highscoreentry::IsPanelVisible(){
	orxBOOL isEnabled = orxFALSE;
	isEnabled = orxObject_IsEnabled(highScoreEntryPanelObject);
	return isEnabled;
}

orxBOOL highscoreentry::IsComplete(){
	if (initialIndex > 2){
		return orxTRUE; 
	}
	return orxFALSE;
}


void highscoreentry::NextLetter(){
	if (initialIndex > 2){
		return; //editing name is over.
	}
	
	alphabetIndex++;
	
	if (alphabetIndex > 26){
		alphabetIndex = 0;
	} 
	UpdateInitialTextObject();
}

void highscoreentry::PrevLetter(){
	if (initialIndex > 2){
		return; //editing name is over.
	}
	
	orxLOG("Index is: %d", alphabetIndex);
	alphabetIndex--;
	
	if (alphabetIndex < 0){
		alphabetIndex = 26;
	}
	orxLOG("Index is now: %d", alphabetIndex);
	
	UpdateInitialTextObject();
}

void highscoreentry::ClearFX(orxOBJECT *object){
	orxObject_RemoveFX(object, "HighScoreLetterSelectedPulseFXSlot");	
	orxObject_AddFX(object, "ClearHighScoreLetterFXSlot");
}

void highscoreentry::UpdateInitialTextObject(){
	orxCHAR character;
	character = GetCharAtIndex(alphabetIndex);
	//orxString_NPrint(character, sizeof(character)-1, "%s", GetCharAtIndex(alphabetIndex));
	orxCHAR stringChar[2] = {};
	stringChar[0] = character;
	
	
	if (initialIndex == 0){
		//orxString_NPrint(chaa, 1, "%s\0", character);
		orxObject_SetTextString(highScoreInitials1Object, stringChar); //(orxCHAR*)alphabet[3] //(orxCHAR*)character
		initial1 = character;
	} else if (initialIndex == 1){
		orxObject_SetTextString(highScoreInitials2Object, stringChar); //(orxCHAR*)alphabet[3] //(orxCHAR*)character
		initial2 = character;
	} else {
		orxObject_SetTextString(highScoreInitials3Object, stringChar); //(orxCHAR*)alphabet[3] //(orxCHAR*)character
		initial3 = character;
	}
}

void highscoreentry::ResetIndicator(){
	if (orxConfig_HasSection("TitleHighScoreInitialIndicator") && orxConfig_PushSection("TitleHighScoreInitialIndicator")) {
		orxVECTOR position = orxVECTOR_0;
		orxConfig_GetVector("Position", &position);
		orxObject_SetPosition(highScoreInitialIndicatorObject, &position);

		orxConfig_PopSection();
	}

}

void highscoreentry::ShiftIndicatorRight(){
	orxVECTOR position = orxVECTOR_0;
	orxObject_GetPosition(highScoreInitialIndicatorObject, &position);
	position.fX += 60;
	
	orxObject_SetPosition(highScoreInitialIndicatorObject, &position);
}

void highscoreentry::NextInitial(){
	initialIndex++;
	alphabetIndex = 0;
	
	if (initialIndex == 0){
		orxObject_AddFX(highScoreInitials1Object, "HighScoreLetterSelectedPulseFXSlot");	
	}
	if (initialIndex == 1){
		ClearFX(highScoreInitials1Object);
		orxObject_AddFX(highScoreInitials2Object, "HighScoreLetterSelectedPulseFXSlot");
		ClearFX(highScoreInitialIndicatorObject);
		orxObject_AddFX(highScoreInitialIndicatorObject, "HighScoreLetterSelectedPulseFXSlot");
		ShiftIndicatorRight();	
	}
	if (initialIndex == 2){
		ClearFX(highScoreInitials2Object);
		orxObject_AddFX(highScoreInitials3Object, "HighScoreLetterSelectedPulseFXSlot");
		ClearFX(highScoreInitialIndicatorObject);
		orxObject_AddFX(highScoreInitialIndicatorObject, "HighScoreLetterSelectedPulseFXSlot");
		ShiftIndicatorRight();	
	}
	if (initialIndex == 3){
		ClearFX(highScoreInitials3Object);
		orxObject_Enable(highScoreInitialIndicatorObject, orxFALSE);
	}
}

orxSTRING highscoreentry::GetInitials(){
	//orxCHAR name[4] = {}; 	
	orxString_Print(name, "%c%c%c", initial1, initial2, initial3);
	
	return name;
}

