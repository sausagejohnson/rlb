#include "orx.h"
#include "beers.h"

beers::beers()
{
	_startHour = 0;
	_endHour = 0;
	_totalBeersToWin = 0;
	_hasJustWonABeer = orxFALSE;

	beerPanelObject = orxObject_CreateFromConfig("BeerPanelObject");
	HideWinPanel();
//	xVisible = 0;
//	xHidden = 1280;
	
	
	orxMath_InitRandom((orxS32)orxSystem_GetRealTime());

	//retrieve and set all values from the config:
	orxBOOL hasBeerSection = orxConfig_HasSection("Beers");

	if (hasBeerSection) {
		if (orxConfig_PushSection("Beers")) {
			_startHour = orxConfig_GetU32("StartHour");
			_endHour = orxConfig_GetU32("EndHour");
			if (_endHour <= 2){
				_endHour = _endHour + 24;
			}
			_totalBeersToWin = orxConfig_GetU32("TotalBeersToWin");
			orxConfig_PopSection();
		}
	}
	
	//if totalbeers = 0, disactivate, but perhaps should keep an vector?
	if (_totalBeersToWin == 0){
		return;
	}
	
	//calculate beer times and create n beers.
	//endtime - starttime / beerstotal, ie 21 - 19 = 2 / 3 = 0.6666
	orxFLOAT timeSlice = ((orxFLOAT)_endHour - (orxFLOAT)_startHour) / (orxFLOAT)_totalBeersToWin;  //0.666
	//orxFLOAT minuteSlices = 60 * timeSlice; //40 minutes ?Not used?
	
	for (int i=0; i<_totalBeersToWin; i++){
		orxFLOAT startBeer = (orxFLOAT)_startHour + (timeSlice * (orxFLOAT)i); //ie 17.666
		orxFLOAT endBeer = (orxFLOAT)_endHour; //default if at array end like 19.000
		if (i+1 < _totalBeersToWin){
			orxFLOAT nextIndex = (orxFLOAT)i+1;
			endBeer = (orxFLOAT)_startHour + (timeSlice * nextIndex); //ie 18.333
		} 
		orxFLOAT randomBeerMarker = orxMath_GetRandomFloat(startBeer, endBeer); //ie 18.2
		//17.000000 to 17.666666, make it: 17.223928
		//17.223928 - 17.0 = 0.223928, 60 * result = minutes
		//orxFLOAT div = ( randomBeerMarker - (orxFLOAT)startBeer); //normalise, ie: 0.534  ???? not used?
		//orxFLOAT minutes = minuteSlices / (timeSlice / div); ?not used????


		std::time_t time = ConvertToTime(randomBeerMarker);
		std::tm tm = *std::localtime(&time);
	
		/*orxLOG("beer from %f to %f, make it: %f", startBeer, endBeer, randomBeerMarker );
		orxLOG("beer win at %d:%d",  (int)randomBeerMarker, (int)minutes);
		orxLOG("correct win at %d:%d", tm.tm_hour, tm.tm_min);*/
		
		beer *beerToWin = new beer(tm.tm_hour, tm.tm_min);
		
		beersAvailable.push_back(beerToWin);
		
	}
	
}

//Loop through all beers checking the time against current time.
//If beer found, mark that beer as won, and return true;
orxBOOL beers::HasJustWonABeer(){
	
// Change of mind.. allow wins to continue, but don't honour them of the winpanel is visible on the ourside
//	if (IsWinPanelVisible() == orxTRUE){
//		return orxFALSE; //don't allow further wins while a win is in progress.
//	}
	
	std::tm currentTime = GetCurrentLocalTime();
	
	for(unsigned int x=0; x<beersAvailable.size(); x++){
		beer *beerToCheck = beersAvailable[x];
		//orxLOG("Testing local: %d:%d, against %d:%d", currentTime.tm_hour, currentTime.tm_min, beerToCheck->Hour(), beerToCheck->Minute());
		if (beerToCheck->HasBeenWon() == orxFALSE && beerToCheck->Hour() == currentTime.tm_hour && beerToCheck->Minute() == currentTime.tm_min ){
			beerToCheck->SetAsWon();
			return orxTRUE;
		}
	}
	
	return orxFALSE;
}

//pass a value like 18.234 and convert to a time using _startHour and _endHour
std::time_t beers::ConvertToTime(orxFLOAT selectedValue){
	orxFLOAT __startHour = (orxFLOAT)_startHour;
	orxFLOAT __endHour = (orxFLOAT)_endHour;
	orxFLOAT normalisedEnd = __endHour - __startHour;
	orxFLOAT normalisedValue = selectedValue - __startHour;
	
	orxFLOAT percentOfWholeRange = normalisedValue / normalisedEnd;
	orxFLOAT minutesOfWholeRange = normalisedEnd * 60;
	
	orxFLOAT minutesOfValue = minutesOfWholeRange * percentOfWholeRange;
	
	//now add these minutes to starttime
	std::time_t time = AddMinutesToStartTime((int)minutesOfValue);
	return time;
}


//std::time_t 
std::time_t beers::AddMinutesToStartTime(int minutes){
	std::time_t startTime = (60 * 60 * 13) +  (60 * 60 * _startHour); //starttime
	std::time_t addedMinutes = startTime + (60 * minutes);
	return addedMinutes;
}


beers::~beers()
{
}

std::tm beers::GetCurrentLocalTime(){
	std::time_t t = std::time(NULL);
    std::tm tm = *std::localtime(&t);
    return tm;
}

void beers::HideWinPanel(){
	//orxVECTOR hiddenPosition = {xHidden, 0, -0.2};
	//orxObject_SetPosition(beerPanelObject, &hiddenPosition);
	orxObject_EnableRecursive(beerPanelObject, orxFALSE);
}

void beers::ShowWinPanel(){
//	orxVECTOR hiddenPosition = {xVisible, 0, -0.2};
//	orxObject_SetPosition(beerPanelObject, &hiddenPosition);
	orxObject_EnableRecursive(beerPanelObject, orxTRUE);
}

//Clear the current win.
orxBOOL beers::IsWinPanelVisible(){
//	orxVECTOR position = {0,0,0};
//	orxObject_GetPosition(beerPanelObject, &position);
//	
//	return position.fX == xVisible;
	return orxObject_IsEnabled(beerPanelObject);
	
}


beer::beer(int hour, int minute)
{
	_hour = hour;
	_minute = minute;
	_hasBeenWon = orxFALSE;
}

beer::~beer()
{
}

int beer::Hour(){
	return _hour;
}

int beer::Minute(){
	return _minute;
}

void beer::SetAsWon(){
	_hasBeenWon = orxTRUE;
}

orxBOOL beer::HasBeenWon(){
	return _hasBeenWon;
}
