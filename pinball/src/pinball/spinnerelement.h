#ifndef SPINNERELEMENT_H
#define SPINNERELEMENT_H

class spinnerelement : public element
{
private:
	orxDOUBLE topBodyHitTime = 0;
	orxDOUBLE midBodyHitTime = 0;
	orxDOUBLE bottomBodyHitTime = 0;

	orxSTRING upEventName;
	orxSTRING downEventName;

	orxFLOAT MAX_SPEED;
	orxFLOAT currentSpeed;
	
	void SpinUp();
	void SpinDown();
	
public:
	spinnerelement(int p, orxSTRING upDirectionEventName, orxSTRING downDirectionEventName, orxFLOAT rotateActivator); //narrow to make the bodies closer;
	~spinnerelement();
	void RunRules();
	void SlowDownTheSpinner();
	int RegisterHit(orxSTRING bodyNameHit);
	
};

#endif // SPINNERELEMENT_H
