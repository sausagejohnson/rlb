#include "orx.h"
#include "element.h"
#include "rolloverlightelement.h"

rolloverlightelement::rolloverlightelement(int points) : element(points, (orxCHAR*)"RolloverLightObject", orxNULL) {
	activatorHitAnimationName = (orxCHAR*)"RolloverLightOnAnim";
	activatorAnimationName = (orxCHAR*)"RolloverLightOffAnim";
	lightOnAnimationName = orxNULL;
	activatorFlashAnimationName = (orxCHAR*)"RolloverLightFlashAnim";
}

rolloverlightelement::~rolloverlightelement(){

}

